# 小书匠语法

## 标准语法

### [斜体](Syntax/standard/italic.md)

### [粗体](Syntax/standard/bold.md)

### [图片](Syntax/standard/image.md)

### [链接](Syntax/standard/link.md)

### [章节标题](Syntax/standard/headline.md)

### [HTML](Syntax/standard/html.md)

### [代码](Syntax/standard/code block.md)

### [无序列表](Syntax/standard/ul.md)

### [有序列表](Syntax/standard/ol.md)

### [引用](Syntax/standard/quote.md)

### [段落](Syntax/standard/section.md)

### [水平线](Syntax/standard/hr.md)

## 高级语法

### [表格](Syntax/advanced/table.md)

### [大纲](Syntax/advanced/outline.md)

### [下标](Syntax/advanced/subscript.md)

### [上标](Syntax/advanced/superscript.md)

### [缩写定义](Syntax/advanced/abbr define.md)

### [定义](Syntax/advanced/define.md)

### [mathjax](Syntax/advanced/mathjax.md)

### [流程图](Syntax/advanced/flow.md)

### [序列图](Syntax/advanced/sequence.md)

### [emoji](Syntax/advanced/emoji.md)

### [居中](Syntax/advanced/center.md)

### [对齐](Syntax/advanced/align.md)

### [html属性修改之decorate](Syntax/advanced/decorate.md)

### [html属性修改之attrs](Syntax/advanced/attrs.md)

### [自定义class](Syntax/advanced/class.md)

### [todo待办](Syntax/advanced/task list.md)

### [脚注](Syntax/advanced/footnote.md)

## 小书匠专用语法

### [思维脑图](Syntax/exclusive/mind map.md)

### [中文斜体](Syntax/exclusive/Italic.md)

### [nunjucks模板引擎](Syntax/exclusive/nunjucks.md)

### [codeChunk代码执行](Syntax/exclusive/chunk.md)

### [远程图片](Syntax/exclusive/image.md)

### [plantuml建模](Syntax/exclusive/plantuml.md)

### [wavedrom数字时间图](Syntax/exclusive/wavedrom.md)

### [mermaid](Syntax/exclusive/mermaid.md)

### [plot统计图](Syntax/exclusive/plot.md)

### [video视频](Syntax/exclusive/video.md)

### [audio音频](Syntax/exclusive/audio.md)

### [attachment附件](Syntax/exclusive/attachment.md)

### [高亮标记](Syntax/exclusive/highlight.md)

### [印刷字替换](Syntax/exclusive/printing.md)

### [注音](Syntax/exclusive/phonetic notation.md)

### [图片大小](Syntax/exclusive/resize.md)

### [wiki链接](Syntax/exclusive/wiki.md)

## 使用教程

### [第三方存储](Syntax/tutorial/storage.md)

### [图片](Syntax/tutorial/image.md)

### [内置编辑器](Syntax/tutorial/built-in editor.md)

### [子编辑器](Syntax/tutorial/child editor.md)

### [过滤器](Syntax/tutorial/filter.md)

### [文章置顶](Syntax/tutorial/stick.md)

### [内部链接](Syntax/tutorial/link.md)

### [链接关系图](Syntax/tutorial/link diagram.md)

### [快捷键](Syntax/tutorial/shortcut keys.md)

### [命令](Syntax/tutorial/command.md)

### [片段](Syntax/tutorial/snippet.md)

### [片段脚本参数](Syntax/tutorial/script.md)

### [待办列表视图](Syntax/tutorial/task.md)

### [预览](Syntax/tutorial/preview.md)

### [表格组件](Syntax/tutorial/table module.md)

### [表格组件编辑器](Syntax/tutorial/table module editor.md)

### [绘图drawio](Syntax/tutorial/drawio.md)

### [发送邮件](Syntax/tutorial/email.md)

### [导入导出](Syntax/tutorial/import export.md)

### [文件管理](Syntax/tutorial/file explorer.md)

### [元数据](Syntax/tutorial/metadata.md)

### [数据中心](Syntax/tutorial/DC.md)