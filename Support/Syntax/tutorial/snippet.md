---
title: 小书匠片段使用说明
tags: 片段, 使用说明,小书匠
renderNumberedHeading: true
slug: /tutorial/snippet
grammar_code: true
grammar_codeLinenums: true
---

[toc]


# 概述

小书匠从 **7.9.7** 版本开始，支持**片段**功能。通过**片段**功能，用户可以快速输入一段预设置的文字。该功能与小书匠的**模板**类似，支持带脚本的片段。不同的是，**片段**是在当前文章鼠标位置处，添加这段文字。

# 片段类型

## 基础片段

基础片段是在用户还没有创建任何片段，或者没有设定任何默认片段时，所使用的片段。同时基础片段也是用户创建普通片段时使用的模板。基础片段只提供正文的片段内容。基础片段同时也是脚本片段和通用片段。

## 脚本片段

脚本片段提供了在使用片段时，允许先执行片段里的脚本，然后再输入脚本生成后的文字。基础片段默认为脚本片段，普通片段需要用户在创建或修改时，通过选项确认是否具有执行脚本功能。

## 默认片段

用户在创建多个普通片段后，可以选择其中的一个片段，做为默认片段。设定默认片段后，用户通过快捷键绑定命令输入片段时，如果指定的片段 id 不存在或者没有指定片段 id 时，将自动使用默认片段来插入内容。

## 普通片段

普通片段就是用户自己创建的片段。普通片段提供了比基础片段更多的片段字段，比如标题等。普通片段又可以分成 **通用片段**, **Ace 编辑器片段**, **Codemirror 编辑器片段** 三种。


### 通用片段

该片段在任何内置编辑器下都可以正常使用，但该片段不提供特定编辑器的片段增强功能。

### Ace 编辑器片段

该片段在 Ace 编辑器下可以正常使用， 同时该片段还支持 Ace 编辑器对片段功能增强的效果，比如片段内的占位符效果。具体语法可以查看后面的详细使用说明

### Codemirror 编辑器片段

该片段在 CodeMirror 编辑器下可以正常使用， 同时该片段还支持 Codemirror 编辑器对片段功能增强的效果，比如片段内的占位符效果。具体语法可以查看后面的详细使用说明

# 片段管理

用户可以通过 `小书匠主按钮>片段` 进入片段管理页面。片段管理页面提供了创建,修改，删除片段，以及设定系统默认片段。

![片段管理](./images/1578624335619.png)


## 片段同步

小书匠为付费用户提供了片段同步功能，只要在小书匠用户管理页面下，打开 `实时同步客户端配置`，系统将会自动同步用户创建的片段。需要注意的是，用户管理页面下提供的配置参数导出不包含片段内容，有需要片段导出，导入功能的，需要在片段管理页面下操作。

## 片段导入，导出

小书匠除了提供片段同步功能外，还提供了片段导入导出功能，方便用户保存自己的片段，同时也可以将自己创建的片段分享给其他用户。片段导入，导出可以在片段管理页面下操作。

# 使用片段

## 通过按钮使用片段

将鼠标移到工具栏上的**输入时间按钮** <kbd><i class="fas fa-clock"></i></kbd>，系统会弹出下拉框，按使用频率列出所有片段，选择想要的片段即可。


## 通过命令使用片段

用户也可以通过[快捷键](http://soft.xiaoshujiang.com/docs/tutorial/shortcuts/)绑定 `insertSnippet` [命令](http://soft.xiaoshujiang.com/docs/tutorial/commands/)，快速输入一个指定的片段来输入内容。

`insertSnippet` 命令需要指定一个片段 `id` 做为参数，所谓 `id` ，就是在片段列表里，片段标题旁边的那一串字符（比如`snippet_1578537460214`）。

![片段id](./images/1578627064184.png)

下面的截图就是把 <kbd><kbd>Ctrl</kbd> + <kbd>.</kbd></kbd> 按键绑定到 `insertSnippet` 上的示例。

![快捷键绑定命令](./images/1578627182518.png)

如果绑定的命令没有带任何参数，或者所带的参数值在片段列表里找不到对应的 id, 系统将使用默认片段，如果用户没有设定默认片段，则直接使用基础片段。

## 片段脚本

### 片段脚本语法

小书匠使用 [underscorejs](http://underscorejs.org/#template) 来执行片段内的脚本，理论上用户可以使用所有系统内置的 javascript 脚本函数。

### 片段脚本示例

可以通过 [moment](https://github.com/moment/moment) 插件生成一个当前日期，时间的片段内容。

``` django
当前时间是 <% print(moment().format('YYYY年MM月DD日 HH时mm分ss秒'))%>
```

注
脚本片段的使用需要有一定的 javascript 语法知识


## Ace 编辑器片段语法

下面的内容引用自[定里](https://cloud9-sdk.readme.io/docs/snippets#section-creating-a-snippet)

### Insertion Points

After inserting the snippet text the cursor will move to the first position after the inserted text, when there are no insertion points specified. Specifying insertion points using the `$` character and a number to specify their sequence.

```
${1}
${2}
etc
```

The `${0}` insertion point is always last in line. You can specify the text that is selected when jumping from one insertion point to the next one.

```
${1:some_text}
```

In addition, it's possible to reuse the same variable multiple times. All instances will be selected using multiple cursors and changed at the same time when the user types.

```
for (var ${1:i}=0; $1 < ${2}; $1++) {
    ${0}
}
```

### Using Special Variables

When triggering a snippet through a menu or command you can configure it to use the text selected prior to inserting the snippet in the resulting code.

```
(function(${1}) {
    ${0:${SELECTED_TEXT:/* code */}}
}(${1}));
```

Similarly the following variables can be used to insert other special text.

| CURRENT_WORD | The word at the cursor at the. |
| --- | --- |
| SELECTION | The selected text. |
| SELECTED_TEXT | The selected text. |
| CURRENT_LINE | The current line. |
| PREV_LINE | The previous line. |
| LINE_INDEX | The index of the current line. |
| LINE_NUMBER | The line number (index + 1). |
| SOFT_TABS | "YES" or "NO". |
| TAB_SIZE | The tab size. |


## Codemirror 编辑器片段语法

下面的内容引用自[这里](https://github.com/angelozerr/CodeMirror-XQuery/wiki/Completion-with-Templates)

```
for (var ${index} = 0; ${index} < ${array}.length; ${index}++) {
var ${array_element} = ${array}[${index}];
${cursor}
}
```

code snippet is inserted in the editor :

![1](./images/1578628643389.png)

The first variable _index_ is selected. If you modify it, it modifies the other _index_ variables :

![2](./images/1578628643589.png)

If you key **Tab**, you select the next variable to replace (here array) that you can replace it too :

![3](./images/1578628644269.png)

To quit the template mode, use **Esc** or type any code in the editor :

![4](./images/1578628648939.png)