---
title: 小书匠文章置顶功能使用说明
tags: 文档,小书匠,置顶,使用说明
createDate: 2020-05-06
renderNumberedHeading: true
slug: /tutorial/article_pinned
---

[toc]

# 概述

小书匠从 `8.0.10` 版本开始支持文章置顶, 置顶文章过滤器功能。通过该功能，用户可以将需要经常修改的文章置顶显示到文件列表的最顶部.置顶后的文章,按置顶时间逆序显示最近 5 篇文章.


# 使用

## 如何使用置顶和取消置顶功能

### 置顶

1. 在文章列表位置,点击鼠标右键,选择置顶

![设置置顶](./images/1588730890829.png)

2. 完成置顶后,文章会被放置在文章列表的最上层

![置顶完成](./images/1588731021520.png)

### 取消置顶

1. 在已经置顶的文章节点,右键选择取消置顶

![取消置顶](./images/1588731188759.png)

## 如何使用文章置顶过滤器

具有使用 [过滤器](http://soft.xiaoshujiang.com/docs/tutorial/filter/) 功能的用户,还可以使用 `文章置顶过滤器` 功能,来显示所有被置顶的文章.

![文章置顶过滤器](./images/1588731536289.png)

# 注

1. 文章被置顶后, 每页最多显示 5 条额外的置顶文章,如果当前的文章列表里也包含有置顶文章,最终显示的置顶条数会多于 5 条.
2. 搜索或者使用文章置顶过滤器显示的文章列表,不显示额外的置顶文章.