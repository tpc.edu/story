---
title: 小书匠片段脚本上下文参数使用说明
tags: 文档,小书匠,片段,使用说明
createDate: 2020-05-06
renderNumberedHeading: true
slug: /tutorial/snippet_script_context
---


[toc]

# 概述

小书匠从 `8.0.11` 版本开始支持脚本片段添加执行上下文参数功能.通过该功能,用户可以在执行脚本片段时,将当前文章的属性(创建时间,文章标题等),剪切板内的内容,及更多参数输出到文章上.

在阅读该文档时,建议先阅读下小书匠的[片段功能](http://soft.xiaoshujiang.com/docs/tutorial/snippet/) 及[按键绑定功能](http://soft.xiaoshujiang.com/docs/tutorial/shortcuts/)

# 使用

1. 在创建片段时,必须选择 `是否支持脚本` 功能打开.

![支持脚本](./images/1589088001301.png)

2. 在片段正文中,输入带脚本的片段. 脚本使用 [underscore.js](https://underscorejs.org/) 的 [template](https://underscorejs.org/#template) 模板引擎

![带脚本的片段内容](./images/1589088174227.png)

3. 完成

# 片段支持的参数

## docTtle

当前文章的标题

## docCreateDate

当前文章的创建时间

## docUpdateDate

当前文章的上次保存时间

## docTagNames

当前文章的标签

## docId

当前文章的内部 id

## docContent

当前文章的内容

## editorSelectionText

当前编辑器内选择的文本内容

## clipboardText

当前剪切板内的纯文本内容(仅客户端)

## clipboardHtml

当前剪切板内的富文本内容(仅客户端)

## clipboardMarkdown

当前剪切板内如果包含富文本内容时,同时提供一份转化成 markdown 的内容 (仅客户端)

# 示例

## 列出所有参数内容

![示例1](./images/1589090133801.png)

```
title: <%=docTitle%>
id: <%=docId%>
createDate: <%=docCreateDate%>
updateDate: <%=docUpdateDate%>
tagNames: <%=docTagNames%>
selectionText: <%=editorSelectionText%>
clipboardText:<%=clipboardText%>
clipboardHtml:<%=clipboardHtml%>
clipboardMarkdown: <%=clipboardMarkdown%>
```

## 自动判断剪切板内容,并执行不同输出

下面的片段演示当剪切板内的内容是超链接时,执行该片段自动将超链接转换成 markdown 格式的超链接.如果用户还选中文本时,自动将选中的文本做为超链接的文字.

该片段需要用户开启 `执行脚本` 和 片段类型选择 `codemirror`

![片段示例二](./images/1589090087231.png)

```
<% if ( /^http[s]?:\/\//i.test(clipboardText)) { 
print("["  + editorSelectionText + "${cursor}](" + clipboardText + ")");
} else {%><%=clipboardText%>${cursor}<%}%>
```
