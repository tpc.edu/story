---
title: 小书匠自定义快捷键说明
tags: 小书匠,快捷键,使用手册
slug: /tutorial/shortcuts
renderNumberedHeading: true
grammar_code: true
---


[toc]

# 概述

小书匠从 `7.8.7` 版本起，开始支持自定义快捷键功能。通过自定义快捷键，配合小书匠提供的内置命令，可以让用户编辑效率大幅度提升，达到事半功倍的效果。



# 快捷键管理

![快捷键管理](./images/1571621091290.png)

从**小书匠主按钮>按键**入口，可以进入快捷键管理界面。在该界面下可以管理快捷键与命令的映射关系。

![添加或者修改快捷键](./images/1571621145804.png)

一条命令可以添加多个快捷键映射，但一个快捷键无法执行多条命令。



# 应用内快捷键

## 命令

小书匠提供了多种命令，不同的命令有不同的默认快捷键及监听范围，详细命令可以查看 [小书匠命令说明](http://soft.xiaoshujiang.com/docs/tutorial/commands)。

### ace 编辑器命令

### codemirror 编辑器命令

### 小书匠应用命令

## 监听范围


![监听范围](./images/1571621725015.png)

系统提供的默认快捷键映射都有监听范围限制，**小书匠应用命令**只有在**操作区**，也就是非编辑区，非预览区内按键才会生效。而 **ace 编辑器命令**只有当用户使用了 ace 的内置编辑器，并且当前在编辑区内活动状态下，按键才会生效。 **codemirror 编辑器命令**也是一样的效果 ，只有当用户使用了 codemirror 的内置编辑器，并且当前在编辑区内活动状态下，按键才会生效。

通过自定义快捷键后，用户可以修改监听范围，对于用户新添加的快捷键，默认在编辑区和操作区都能监听到。


### 编辑区

用户输入文字编辑的区域。用户在编辑区下输入或者选择文字等操作，就激活了编辑区。

![编辑区](./images/1571621314482.png)

### 操作区

除去编辑区和预览区，剩下的区域就是操作区，比如文件列表面板， 工具栏等。

![操作区](./images/1571621324220.png)

### 预览区

目前小书匠没有实现预览区自定义快捷键功能，用户在预览区内按下的任何快捷键，都会返回给系统层面进行管理。

比如在 web 版本的小书匠里，在预览区激活状态下(可以在预览区里选中一些文字)，按下 <kbd>ctrl+f</kbd>，这时候只会触发浏览器自身的搜索功能，而不是在编辑区下对应的搜索框。

![预览区](./images/1571621331072.png)

### 全部 

这里的**全部**仅指**编辑区**和**操作区**，不包括**预览区**和**操作系统**。

## 按键定义

![按键定义](./images/1571622733491.png)

### 单键

单键就是只需要按下一个键，就可以触发相应的命令。一般单键由常规的字符键组成，不建议使用  <kbd>ctrl</kbd> <kbd>shift</kbd> <kbd>alt</kbd> <kbd>command</kbd> 这些修饰键做为单键来使用。

过多的使用单键来映射命令，容易造成键盘上的按键不够使用，这时可以考虑使用**组合键**或者**顺序键**。



### 组合键

组合键就是定义了需要同时按下的几个键才能触发相应的命令，组合键需要由 <kbd>ctrl</kbd> <kbd>shift</kbd> <kbd>alt</kbd> <kbd>command</kbd> 中的一个或者多个修饰键配合单键，也就是常见的字符键组成。

### 顺序键

顺序键就是需要在一定时间内，依次按下键后才能触发执行相应的命令。顺序键可以由多个**单键**组成，也可以配合**组合键**来实现。

多个单键的顺序键, 比如 <kbd>g</kbd> <kbd>i</kbd> <kbd>f</kbd>, 当在键盘上顺序按下这些键时，就会触发相应的命令。

单键与组合键结合使用的顺序键， 比如 <kbd>ctrl+f</kbd> <kbd>i</kbd> <kbd>t</kbd>, 需要先同时按下 <kbd>ctrl</kbd> 和 <kbd>f</kbd> 两个键，然后松开，再依次按下 <kbd>i</kbd> 和 <kbd>t</kbd> 两个键才会触发相应的命令。

**注:** 如果定义了组合键映射命令，然后定义的顺序键映射命令里按键定义包含了组合键按键定义，则组合键对应的映射命令将失效。比如 <kbd>ctrl+s</kbd> 组合键映射命令为 **save**， 然后定义了顺序键 <kbd>ctrl+s</kbd> <kbd>f</kbd> 映射命令为 **saveAs**，则 <kbd>ctrl+s</kbd> 的组合键将失效，无法执行 **save** 命令。


#### 如何定义一个顺序按键

在定义完一个**单键**或者**组合键**后，等待一秒左右的时间，在按键录制区的左侧出现刚刚录制的按键信息后，就可以继续在按键录制区定义下一个顺序按键了。

![定义一个顺序按键](./images/1571623524029.png)


# 操作系统快捷键

操作系统快捷键和应用快捷键不同，操作系统定义的快捷键映射的命令只能是小书匠客户端命令，无法使用其他命令，同样的，应用快捷键映射的命令只能是非小书匠客户端命令。

操作系统快捷键有最高的优先级，如果同时定义了操作系统快捷键和应用快捷键，将只会触发操作系统快捷键。


比如定义了操作系统范围的 <kbd>ctrl+shift+h</kbd> 按键到 **toggleWindow** 命令，同时定义了应用范围的 <kbd>ctrl+shift+h</kbd> 按键到 **xsj_heading** 命令，当按下快捷键 <kbd>ctrl+shift+h</kbd>, 只会执行 **toggleWindow** 命令。

操作系统快捷键并不需要当前小书匠处理活动状态，在小书匠窗口最小化，或者操作其他应用时，按下预先定义的快捷键，也会触发该快捷键对应的命令。（需要该操作系统快捷键不与其他应用冲突）

操作系统快捷键只支持单键或者组合键，不支持顺序键。

## 小书匠客户端命令

操作系统级的快捷键只在小书匠客户端版本进行支持，用户每次修改后，需要重新启动小书匠来让快捷键生效。

定义操作系统快捷键对应的命令只能是小书匠客户端命令，其他命令(比如小书匠应用命令，ace 编辑器命令, codemirror 编辑器命令)无效。

所有小书匠客户端命令默认都没有快捷键，需要用户定义后才能生效。

详细的客户端命令，可以参考[命令说明](http://soft.xiaoshujiang.com/docs/commands)文档。


## 如何定义一个操作系统快捷键

1. 在小书匠客户端下，快捷键管理界面的左下角有一个**定义系统级按键**

![操作系统快捷键](./images/1571629417209.png)

2. 在操作系统管理界面下定义快捷键

![操作系统定义快捷键](./images/1571629479759.png)

操作系统快捷键的定义与应用内的快捷键定义不同，除了每次修改完成后，需要重启才能生效外，操作系统快捷键定义需要用户输入合法的 json 数据格式来完成定义。

``` json?title=操作系统快捷键定义格式
{
  ">>==操作系统命令1==<<": ">>==定义的快捷键1==<<",
  ">>==操作系统命令2==<<": ">>==定义的快捷键2==<<"
}
```

示例

将 <kbd>ctrl+shift+h</kbd> 快捷键定义成隐藏或者显示窗口

``` json
{
  "toggleWindow": "Ctrl+Shift+H"
}
```

## 按键名称

操作系统快捷键需要自己定义快捷键信息，没有像应用快捷键定义那样，提供一个按键录制功能。下面列出了操作系统快捷键支持的按键名称


1. 修饰按键 

* <kbd>Ctrl</kbd>
* <kbd>Alt</kbd>
* <kbd>Shift</kbd>
* <kbd>Command</kbd>: <kbd>Command</kbd> 对应苹果系统的 (<kbd>⌘</kbd>) 按键与 Windows / Linux 的 <kbd><i class="fab fa-windows"></i></kbd> 按键

2. 其他支持的按键

 - Alphabet: <kbd>A</kbd>-<kbd>Z</kbd>
 - Digits: <kbd>0</kbd>-<kbd>9</kbd>
 - Function Keys: <kbd>F1</kbd>-<kbd>F24</kbd>
 - <kbd>Comma</kbd>
 - <kbd>Period</kbd>
 - <kbd>Tab</kbd>
 - <kbd>Home</kbd> / <kbd>End</kbd> / <kbd>PageUp</kbd> / <kbd>PageDown</kbd> / <kbd>Insert</kbd> / <kbd>Delete</kbd>
 - <kbd>Up</kbd> / <kbd>Down</kbd> / <kbd>Left</kbd> / <kbd>Right</kbd>
 - <kbd>MediaNextTrack</kbd> / <kbd>MediaPlayPause</kbd> / <kbd>MediaPrevTrack</kbd> / <kbd>MediaStop</kbd>
 - <kbd>Comma</kbd> or <kbd>,</kbd>
 - <kbd>Period</kbd> or <kbd>.</kbd>
 - <kbd>Tab</kbd> or <kbd>\t</kbd>
 - <kbd>Backquote</kbd> or <kbd>`</kbd>
 - <kbd>Enter</kbd> or <kbd>\n</kbd>
 - <kbd>Minus</kbd> or <kbd>-</kbd>
 - <kbd>Equal</kbd> or <kbd>=</kbd>
 - <kbd>Backslash</kbd> or <kbd>\\</kbd>
 - <kbd>Semicolon</kbd> or <kbd>;</kbd>
 - <kbd>Quote</kbd> or <kbd>'</kbd>
 - <kbd>BracketLeft</kbd> or <kbd>[</kbd>
 - <kbd>BracketRight</kbd> or <kbd>]</kbd>
 - <kbd>Escape</kbd>

# 疑问

