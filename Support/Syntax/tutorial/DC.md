---
title: 小书匠数据中心使用说明
tags: 数据中心,教程,小书匠
grammar_plantuml: true
slug: /tutorial/database
---

[toc]

# 概述

``` plantuml!
!include <tupadr3/common>

!include <office/Servers/database_server>
!include <office/Servers/application_server>
!include <office/Concepts/firewall_orange>
!include <office/Clouds/cloud_disaster_red>
!include <office/Clouds/cloud>
!include <office/Clouds/public_cloud>
!include <office/Devices/device_laptop>
!include <office/Devices/device_lcd_monitor>
!include <office/Devices/device_mac_client>
!include <office/Concepts/website>
!include <office/Concepts/documents>
!include <tupadr3/font-awesome/file_word_o>
!include <tupadr3/font-awesome-5/cogs>
!include <tupadr3/font-awesome-5/dropbox>
!include <tupadr3/font-awesome-5/github>
!include <tupadr3/font-awesome-5/gitlab>
top to bottom direction
OFF_DEVICE_LCD_MONITOR(client2,小书匠客户端)
OFF_DEVICE_MAC_CLIENT(client3,小书匠客户端)
OFF_DEVICE_LAPTOP(clientmore,小书匠客户端...)
OFF_WEBSITE(clientweb, 小书匠web版)
OFF_WEBSITE(clientweb1, 小书匠web版)



node "<$device_laptop>\n小书匠客户端" as "clineta" {
database "本地数据库"  as "localdatabase3" {
  OFF_DOCUMENTS(localdocs1, json文件) #RoyalBlue
}
FA5_COGS(operate, 编辑器操作)
  [operate] -down- [localdocs1]:文章的每次修改\n保存到本地数据库
} 
cloud "<$public_cloud>\n远程服务器\n" {
  database "远程数据库" {
    OFF_DOCUMENTS(remotedocs, json文件) #RoyalBlue
  }
}

node "<$website>\n小书匠web版" {
database "本地数据库" as "localdatabase2" {
  OFF_DOCUMENTS(localdocs2, json文件) #RoyalBlue
}
FA5_COGS(operate1, 编辑器操作)
  [operate1] -down- [localdocs2]:文章的每次修改\n保存到本地数据库
} 

cloud "<$cloud>\n第三方云服务\n<$dropbox> <$github> <$gitlab> \n" as "remotestore"{
   FA_FILE_WORD_O(remotestoredocs, 第三方文件)  #DeepSkyBlue
}

actor 用户

note left of 用户: 输入
用户 -left-> [operate]
用户 -right-> [operate1]
note right of remotestoredocs: 保存为markdown纯文本文件\n或渲染后的html文件
[operate] <.up.> [remotestoredocs]
[localdocs1] <-down-> [remotedocs]: 实时双向同步
[localdocs2] <-down-> [remotedocs]: 实时双向同步
[operate1] <.up.> [remotestoredocs]

[remotedocs] <-right-> client2
[remotedocs] <-down-> [clientweb]
[remotedocs] <-down-> [clientmore]
[client3] <.up.> [remotestoredocs]
[clientweb1] <.up.> [remotestoredocs]: 手动保存或者读取
```


小书匠内置数据库使用了 [PouchDB][1] ,一款非关系型数据库 NOSQL，非常适合文档数据的存储，又比一般的 NOSQL 数据库，比如 MONOGO ，多提供了数据同步和历史记录功能。

小书匠提出了数据中心的概念，用来帮助用户通过底层数据的形式更加容易的维护自己的数据安全。目前免费用户只能使用本地数据库进行一般的数据库导出，导入和重置等操作，付费用户可以实现自定义服务器，数据库数据可以实现实时同步的效果。

## Couch 协议

> **The Couch Replication Protocol** lets your data flow seamlessly between server clusters to mobile phones and web browsers, enabling a compelling **offline-first** user-experience while maintaining high performance and strong reliability. CouchDB comes with a developer-friendly query language, and optionally MapReduce for simple, efficient, and comprehensive data retrieval.


## PouchDB

> [PouchDB][1] is an open-source JavaScript database inspired by Apache CouchDB that is designed to run well within the browser.


## CouchDB

> Seamless multi-master sync, that scales from Big Data to Mobile, with an Intuitive HTTP/JSON API and designed for Reliability.

## 数据库和第三方存储的差别

数据库存储的数据格式为最原始的数据而第三方存储可能是转换成 html 后的数据或者纯文本的 markdown 数据。

数据库会在用户每次修改文件时，实时保存用户输入，第三方存储则只有在用户明确保存时，才会同步到第三方平台服务器上。

## 为什么不把第三方存储也做成实时保存

1. 第三方平台 api 限制频繁访问
2. 有些第三方平台需要转换成 html 后才能保存，这个时候进行实时保存需要消耗大量 cpu ，影响用户写作体验

# 本地数据库管理

在**小书匠主按钮>数据>基本** 界面上，可以进行本地数据库管理，包括导入数据库，导出数据库，删除数据库，重建索引，压缩数据库。

![本地数据库界面](./images/1546998930745.png)

## 重置数据库

删除数据库后，系统就会自动将数据库重置为系统初始化时的数据库。

## 压缩数据库

压缩数据库可以清空数据库内的所有历史记录，用来减少数据占用的空间。

用户每次对文章的修改，都会生成一个历史版本，由于 PouchDB 数据库自身的特性，无法对单独文件的历史记录进行删除，只能通过压缩数据库的方式，清空该数据库内部的所有历史记录，仅保存最新版本的文章和有冲突需要用户自己合并的版本内容。

**注: 压缩数据库清空的是所有文章的历史记录。**

## 删除数据库

**删除数据库后，用户在数据库内的所有数据都会被删除，同时数据库重新恢复到系统初始化时的状态。**

该操作仅对本地数据库删除操作有效，配置了数据中心服务器的远程数据库不会受影响，想要删除远程数据库，需要自己到对应的数据库操作界面进行删除操作。需要注意的是，如果配置了实时同步远程数据库功能，系统会在本地数据库重新恢复到初始化状态时，自动同步远程数据库的数据到本地。如果不想同步，需要先取消远程数据库，再进行删除。


# 远程数据库

所谓远程数据库，就是一台专门用来同步数据的数据库服务器，只有该数据库支持 [The Couch Replication Protocol](http://docs.couchdb.org/en/master/replication/protocol.html) 协议，都可以做为远程数据库，实现小书匠本地数据库和远程数据库的同步功能。

目前小书匠对付费用户提供了小书匠自己的远程数据库服务器和用户自己搭建的远程数据库服务器两种入口选项。

## 小书匠临时数据库

![小书匠数据库服务器](./images/1547001917053.png)

小书匠临时数据库目前只对付费用户开放，用户可以通过该数据库，实时同步用户本地数据，方便用户在不同办公地点能够及时保证修改的文件处于最新状态。

![小书匠数据库状态](./images/1547004206994.png)

> **注:** 
> * 小书匠数据中心仅做为临时中转站，方便用户在不同平台,不同办公地之间快速编辑最近几篇文章。
> * 小书匠会不定时清空服务器数据，请不要把小书匠数据服务器做为自己的存储方案,建议把完稿的文章保存到第三方存储平台上
> * 小书匠数据服务器定时清空数据不会操作用户本地客户端(或浏览器)的数据
> * 小书匠只会在启动时双向同步最近修改的10篇文章(包括归档，用户自己的删除操作)，并实时同步启动后用户修改的文章
> * 小书匠不会同步系统创建的手册文章，请不在要这些文章基础上进行修改

## 自定义远程数据库

![自定义数据](./images/1547002064134.png)

付费用户除了可以使用小书匠的临时数据库，还可以自己搭建数据库服务器，设置自己的远程数据库。目前支持 Couch 协议的数据库主要是 [CouchDB][2] 自己的数据库，当然 [PouchDB][1] 自己也实现了服务器功能。

当配置一个自定义远程服务器后，在文件列表面板左上方会显示一个云图标<kbd><i class="fas fa-cloud"></i></kbd>按钮，显示本地数据库与远程数据库的同步状态。用户还可以通过该操作按钮进行更多操作。

![自定义数据库状态](./images/1547004032920.png)




## 数据同步

在配置数据库同步时，系统允许用户选择实时同步或者手动同步，同步数据条数等功能。

该功能操作主要在文件列表面板上方的数据库按钮。

![数据同步](./images/1547004508187.png)

注：同步文章篇数包括归档，删除的文章篇数，所以当进行同步时，可能在文件列表面板上查看到的文章篇数并没有实际配置的同步篇数多。

### 实时同步

开启实时同步功能时，系统默认进行双向同步，并在小书匠系统每次启动时双向同步各自己数据库最近的文章数。

用户可以在自定义数据库配置页面，打开或者关闭实时同步功能，调整每次启动检测最大文章数目。

![实时同步设置](./images/1547004604937.png)

### 手动同步

#### 双向同步

点击<kbd><i class="fas fa-cloud"></i></kbd>按钮，系统将立即执行双向同步，同步文章数量为用户在自定义数据库配置的同步条数。

用户可以在下拉列表里选择全库双向同步。需要注意的是 web 版用户由于浏览器的限制，会有本地数据存储最大的限制，所以不建议在 web 版进行全库双向同步。

![双向同步](./images/1547005293702.png)

#### 拉取

拉取功能允许单向从远程数据库拉取文章到本地数据库，支持全部拉取，拉取最近修改的 10 篇，拉取最近修改的 50 篇文章三种操作

![拉取](./images/1547005388144.png)

#### 推送

推送功能允许单向把本地数据库文章推送到远程数据库，支持全部推送，推送最近修改的 10 篇，推送最近修改的 50 篇文章三种操作

![推送](./images/1547005400452.png)

# 相关

1. [PouchDB 官网][1]
2. [CouchDB 官网][2]


[1]: https://pouchdb.com/
[2]: http://couchdb.apache.org/