---
title: 小书匠过滤器使用说明
tags: 文件列表, 过滤器,小书匠
renderNumberedHeading: true
slug: /tutorial/filter
---

[toc]


# 概述

小书匠从 `7.9.1` 版本开始支持过滤器功能。通过过滤器，用户可以在左侧的文件列表里快速的过滤出相关的文件列表。过滤器支持用户添加自定义，同时系统会根据当前内置数据库的文章，索引出分类过滤器列表，和标签过滤列表，分类过滤器列表还提供了树形显示的效果。

![过滤器](./images/1573360641860.png)


# 过滤器类型

目前系统提供四种过滤器类型，分别是 **系统默认过滤器**,**标签过滤器**,**分类过滤器**,**标识过滤器**。除了**系统默认过滤器**用户无法自己维护外，其他三种过滤器类型，用户都可以在 **添加过滤器** 按钮上添加自定义实现。

## 系统默认过滤器

目前系统仅提供了一个最近修改的过滤器列表，用于显示草稿状态下最近修改过的文件。

## 标签过滤器

按文章元数据 **tags** 值进行过滤的过滤器。

支持多个标签进行过滤，多个标签值之间用逗号分开。同时多个标签过滤时，系统使用的是 **与** 的关系进行过滤，比如你输入的值是 **小书匠,使用说明**。 则过滤出来的文章列表必须同时满足含有 **小书匠** 和 **使用说明** 两个标签的文章。

标签过滤器属于**包含关系**。 只要文章元数据 **tags** 里的值包含了标签过滤器里的值时，该文章就会显示出来。

## 分类过滤器

按文章元数据 **category** 值进行过滤的过滤器。

分类过滤器支持树形显示效果，每个树形层级可以通过 **/** 符号进行分隔。

比如只要在文章元数据 **category** 里，值为 **小书匠/示例**。则在过滤器列表里就会显示成 **小书匠** 与 **示例**

![分类文章](./images/1573358240538.png)

![树形显示分类过滤器列表](./images/1573358113733.png)

### 分类过滤器与 category 值关系

分类过滤器与分类值之间可以有 **等于关系** 和 **起始于关系**。 系统默认使用 **等于关系**。

#### 等于关系

分类过滤器默认属于**等于关系**。文章元数据 **category** 必须完全等于分类过滤器里的值时，才会显示出来。

#### 起始于关系

当分类过滤器设置里的值以 **xsjStartsWith##** 为开头时，分类过滤器与文章里的元数据 **category** 将改变为 **起始于关系**。 比如添加的过滤器值为 **xsjStartsWith##小书匠**，则所有文章里元数据 **category** 以 **小书匠** 开头的文章都将显示在列表里。

![起始于关系](./images/1575081863097.png)



## 标识过滤器

按文章元数据 **flag** 值进行过滤的过滤器。

该元数据值主要是给用户自定义使用的。用户根据自己使用的场景，设定该值的含义。比如可以将该值设定业 **普通**, **重要**, **秘密**, **绝密** 等，然后添加自己的标识过滤器，来快速过滤出这些文章。

标识过滤器属于**等于关系**，文章元数据 **flag** 必须完全等于标识过滤器里的值时，才会显示出该文章。

## 区别

### 标签过滤器与标识过滤器的区别

标识过滤器和标签过滤器不同的地方是，标签过滤器支持文章里的多个标签组合过滤，而标识过滤器是绝对匹配文章里对应的标识值的。标签过滤器是**包含关系**，标识过滤器是**等于关系**。标签过滤器里的值只要包含于文章的 **tags** 值，就会显示该文章。标识过滤器里的值需要和文章里的 **flag** 值一样时，才会显示出来。

### 分类过滤器与标识过滤器的区别

分类过滤器可以是**等于关系** 也可以是 **起始于关系**，当使用默认的 **等于关系** 时，分类过滤器里的值也是必须和文章里的 **category** 一致，才会显示该文章。不同的是系统在默认分类过滤器列表时，提供了按树形显示的效果。

# 过滤器列表

## 自定义过滤器列表

![自定义](./images/1573367040386.png)

该列表下列出了系统默认的过滤器和用户添加的自定义过滤器。用户可以在该列表里将过滤器置顶到工具栏上方，也可以删除自己创建的过器。

## 分类树过滤器列表

![分类树](./images/1573366988100.png)

系统默认会把当前数据库里的文章按分类（元数据 ：category）进行索引，并在分类树过滤器列表里按树形结构显示。

## 标签过滤器列表

![标签](./images/1573367019553.png)

系统默认会把当前数据库内的文章按标签(元数据：tags) 进行索引，并在标签过滤器列表里显示出来。

# 添加自定义过滤器



1. 将鼠标移到文件列表右上方的过滤器<kbd><i class="fas fa-chess"></i></kbd>按钮处，将会弹出过滤器列表维护页面。

![过滤器按钮](./images/1573367391159.png)
2. 点击 **添加新的过滤规则** 按钮，进入添加过滤器页面

![添加过滤器](./images/1573367475412.png)



# 注

1. 系统仅列出最新的 5 个置顶过滤器
2. 目前系统仅显示最近修改的 50 篇文章，如果使用的过滤器对应的文章太多，只会列出最近修改过的 50 篇文章
3. 过滤器过滤的文章不包括被删除的文章，想要找回删除掉的文章需要自己到回收站撤消删除
4. 用户最多添加 50 个自定义过滤器
5. 非会员用户只会列出过滤器最近修改的 5 篇文章
6. 当同步系统或者有新文章创建时，都会把新文章添加到文章列表里，即使该文章不包括当前使用的过滤器内。该功能主要是方便用户切换文章使用，并不表示该新文章内的属性自动变成该过滤器定义的过滤值内。用户重新点击一下这个过滤器，就会刷新文章列表，把这些不属于该过滤器内的文章过滤掉。