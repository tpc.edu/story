---
title: 小书匠支持的命令说明
tags: markdown, 命令,小书匠
renderNumberedHeading: true
slug: /tutorial/commands
---

[toc]


# 概述

小书匠从 `7.8.7` 版本开始支持自定义快捷键功能，相应的开放了部份命令。该文档列出了小书匠目前支持的所有命令。

# 命令

命令是用来执行一定的功能，比如保存，打开预览，选择文字等。

小书匠支持四种命令类型， **小书匠应用命令**， **ace 命令**， **codemirror 命令**， **小书匠客户端命令**

所有命令都有默认生效范围， 小书匠应用命令默认在非编辑区，非预览区内点击想应的默认快捷键，命令才会生效。 ace 命令只有在 使用 ace 编辑器，并且在编辑区内触发快捷键时，才会生效。 codemirror 命令只有在使用 codemirror 编辑器，并且在编辑区内触发快捷键时，才会生效。而小书匠客户端命令，只有在小书匠客户端状态下生效，小书匠 web 版本没有客户端命令。

## 小书匠应用命令

### save

保存操作

如果用户绑定了第三方存储,并指定为默认存储时,会同步触发第三方存储的同步

默认快捷键 <kbd>ctrl+s</kbd>

### saveAs

另存为

默认快捷键 <kbd>ctrl+shift+s</kbd>

### saveInternal

保存操作(与 `save` 命令不同的是,该命令不会触发第三方存储同步)

### preview

打开或者关闭预览

默认快捷键 <kbd>ctrl+shift+p</kbd>

### zenWriter

全屏写作

默认快捷键 <kbd>ctrl+shift+1</kbd>

### zenReader

全屏阅读

默认快捷键 <kbd>ctrl+shift+2</kbd>

### toggleStore

打开文件列表

默认快捷键 <kbd>ctrl+shift+f</kbd>

### toggleSidebar

打开侧边菜单栏

默认快捷键 <kbd>ctrl+shift+b</kbd>

### preventToDefault

禁用该快捷键

### passToDefault

取消快捷键

用于对默认快捷键的取消功能，比如想取消 <kbd>ctrl+s</kbd> 原有的默认绑定，可以添加该命令

### exportFiles

导出文件

### toggleEditorTocPanel

打开编辑区大纲面板

### togglePreviewTocPanel

打开预览区大纲面板


### toggleFileListPanel

打开或者关闭文件列表

默认快捷键 <kbd>ctrl+left</kbd>

### toggleToolPanel

打开或者关闭工具栏

默认快捷键 <kbd>ctrl+up</kbd>

### toggleVimMode

打开或者关闭 vim 模式（需要使用的内置编辑器支持 vim 按键）

### toggleEmacsMode

打开或者关闭 emacs 模式 (需要使用的内置编辑器支持 emacs 按键)

### setFileModelAttribute

修改选中文件的属性

需要指定两个参数，第一个参数为要修改的属性（'tagNames', 'category', 'flag'），第二个参数为对应属性的值，两个参数之间需要用空格分开。

### insertSnippet

快速输入一个片段

需要指定一个片段 id 做为输入参数，如果不指定片段 id或者片段 id 不存在, 系统直接使用默认片段

### xsj_bold

加粗

默认快捷键 <kbd> ctrl+b</kbd>

### xsj_italic

斜体

默认快捷键 <kbd>ctrl+i</kbd>

### xsj_footnote

脚注

### xsj_mark

高亮

默认快捷键 <kbd>ctrl+m</kbd>

### xsj_link

链接

默认快捷键 <kbd>ctrl+l</kbd>

### xsj_moment

输入当前时间

默认快捷键 <kbd>ctrl+.</kbd>

### xsj_html

html 转换成 markdown

### xsj_toc

输入静态大纲

### xsj_magent

图床迁移

### xsj_quote

引用 

默认快捷键 <kbd>ctrl+q</kbd>

### xsj_code

代码块

默认快捷键 <kbd>ctrl+k</kbd>

### xsj_image

插入图片

默认快捷键 <kbd>ctrl+g</kbd>

### xsj_olist

有序列表

默认快捷键 <kbd>ctrl+o</kbd>

### xsj_ulist

无序列表

默认快捷键 <kbd>ctrl+u</kbd>

### xsj_tlist

待办

### xsj_fonticon

图标

### xsj_table

表格 

默认快捷键 <kbd>ctrl+t</kbd>

### xsj_ntable

表格组件

### xsj_drawio

绘图组件

### xsj_hr

水平线

默认快捷键 <kbd>ctrl+r</kbd>

### xsj_undo

撤消上一步操作

默认快捷键 <kbd>ctrl+z</kbd>

### xsj_redo

取消撤消

默认快捷键 <kbd>ctrl+y</kbd>

### xsj_heading

标题

默认快捷键 <kbd>ctrl+h</kbd>



## ace 编辑器命令

详细命令可以参考 ace 编辑器的 [官网](https://github.com/ajaxorg/ace/blob/master/lib/ace/commands/default_commands.js)

## codemirror 编辑器命令

详细命令可以参考  codemirror 的 [官网](https://codemirror.net/doc/manual.html#commands)

## 小书匠客户端命令

### toggleWindow

隐藏或者显示小书匠客户端

### hideWindow

隐藏小书匠客户端

### showWindow

显示小书匠客户端

### lockWindow

锁定小书匠客户端，并最小化到系统托盘

### trayWindow

将小书匠客户端最小化到系统托盘

### unTrayWindow

取消托盘状态



# 疑问

# 相关