---
title: 小书匠语法说明之定义
tags: markdown, 语法, 定义,小书匠
slug: /grammar/advance/deflist/
---

[toc]


# 概述

定义语法不是标准的 commonmark，  不同的编辑器或者博客平台支持可能不一样。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-deflist][2] 实现对定义的支持。

该语法格式也是主要参考了 [pandoc](http://pandoc.org/MANUAL.html#definition-lists) 的定义格式

# 使用

元数据标识: **grammar_deflist**

想要使用该语法，需要在**设置>扩展语法** 里把**定义**选项打开。或者在每篇文章的元数据里通过 **grammar_deflist** 进行控制。系统默认打开定义语法功能

语法结构

```
Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.
```

或者

```
Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b
```

## 示例

```
苹果
: 一种水果
: 一种品牌，计算机，手持设备
桔子
: 一种水果
```



## 效果

苹果

: 一种水果
: 一种品牌，计算机，手持设备

桔子
: 一种水果



## HTML

生成 的 html 片段

```html
<dl>
<dt>苹果</dt>
<dd>一种水果</dd>
<dd>一种品牌，计算机，手持设备</dd>
<dt>桔子</dt>
<dd>一种水果</dd>
</dl>
```


# 疑问

# 相关


1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [pandoc def list](http://pandoc.org/MANUAL.html#definition-lists)


[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-deflist