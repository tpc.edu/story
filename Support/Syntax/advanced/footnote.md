---
title: 小书匠语法说明之脚注
tags: markdown, 语法,脚注,小书匠
grammar_footnote: true
grammar_mathjax: true
slug: /grammar/advance/footnote/
---

[toc]


# 概述

脚注语法不是标准的 commonmark，  不同的编辑器或者博客平台支持可能不一样。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-footnote][2] 实现对上标的支持。

# 使用

元数据标识: **grammar_footnote**

想要使用该语法，需要在**设置>扩展语法** 里把**脚注**选项打开。或者在每篇文章的元数据里通过 **grammar_footnote** 进行控制。系统默认打开**脚注**语法功能

该语法书写格式主要参考了 [pandoc footnote](http://pandoc.org/MANUAL.html#footnotes) 的书写格式

## 内联式脚注

### 示例

```
正文内容[^1]

[^1]: 这是脚注定义.
```

### 效果

正文内容[^1]

[^1]: 这是脚注定义.

## 引用式脚注

### 示例

```
正文内容^[脚注定义内容]
```

### 效果

正文内容^[脚注定义内容]

## HTML

``` html
<p>正文内容<sup class="footnote-ref"><a href="#fn1" id="fnref1">[1]</a></sup></p>
<p>正文内容<sup class="footnote-ref"><a href="#fn2" id="fnref2">[2]</a></sup></p>

<hr class="footnotes-sep">

<div class="footnotes">
<ol class="footnotes-list">
<li id="fn1" class="footnote-item"><p class="xsj_paragraph xsj_paragraph_level_1">这是脚注定义. <a href="#fnref1" class="footnote-backref">↩</a></p>
</li>
<li id="fn2" class="footnote-item"><p class="xsj_paragraph xsj_paragraph_level_0">脚注定义内容 <a href="#fnref2" class="footnote-backref">↩</a></p>
</li>
</ol>
</div>
```

## 多行脚注内容

允许在脚注定义里，进行多行定义，只要在新行里进行至少两个空格或者一个制表符就可以。

只有引用式脚注支持多行脚注内容，内联式脚注无注实现多行脚注内容定义。

### 示例

```
这是一个使用了多行脚注内容的文章.[^longnote]

[^longnote]: 多行脚注定义

    注意前面要有空格
	在脚注里面定义数学公式 `!$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$`

```

### 效果

这是一个使用了多行脚注内容的文章.[^longnote]

[^longnote]: 多行脚注定义

    注意前面要有空格
	在脚注里面定义数学公式 `!$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$`
	
## 重复脚注引用

重复脚注引用指的是一个引用式脚注定义，在文章里多次被引用 。系统对该脚注编号会自动提供子编号功能

### 示例

```
重复脚注引用 [^2]
另一个重复脚注引用 [^2]

[^2]: 脚注定义
```

### 效果

重复脚注引用 [^2]
另一个重复脚注引用 [^2]

[^2]: 脚注定义

# 疑问

1. 脚注本身定义不需要放在文章的结尾，只是在生成 HTML 后系统会自动将所有脚注调至文章结尾处。
2. 脚注里的引用（像前面的 `^1`, `^longnote`）名称，用户可以自行设定，系统在生成 HTML 后，会对编号进行统一处理。

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [markdown-it-footnote 扩展插件][2]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-footnote