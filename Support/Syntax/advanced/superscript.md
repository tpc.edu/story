---
title: 小书匠语法说明之上标
tags: markdown, 语法,上标,小书匠
slug: /grammar/advance/supscript/
---

[toc]


# 概述

上标语法不是标准的 commonmark，  不同的编辑器或者博客平台支持可能不一样。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-sup][2] 实现对上标的支持。

# 使用

元数据标识: **grammar_sup**

想要使用该语法，需要在**设置>扩展语法** 里把**上角文字**选项打开。或者在每篇文章的元数据里通过 **grammar_sup** 进行控制。系统默认打开**上角文字**语法功能

然后将想要进行上标显示的文字包裹在 `^` 符号内就可以。

## 示例

```
上角文字: 29^th^
```

## 效果

上角文字: 29^th^

## hmtl

``` html
29^th^ => 29<sup>th</sup>
```

# 疑问

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [markdown-it-sup 扩展插件][2]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-sup