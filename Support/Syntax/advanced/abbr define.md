---
title: 小书匠语法说明之缩写
tags: markdown, 语法,缩写,小书匠
grammar_abbr: true
grammar_code: true
slug: /grammar/advance/abbr/
---

[toc]


# 概述


缩写语法不是标准的 commonmark，  不同的编辑器或者博客平台支持可能不一样。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-abbr][2] 实现对缩写的支持。


该语法格式也是主要参考了 [php markdown extra](https://michelf.ca/projects/php-markdown/extra/#abbr) 的书写格式



# 使用

元数据标识: **grammar_abbr**

想要使用该语法，需要在**设置>扩展语法** 里把**缩写定义**选项打开。或者在每篇文章的元数据里通过 **grammar_abbr** 进行控制。系统默认打开缩写定义语法功能

书写格式

```
*[缩写的名称]: 全称，详细的说明
```

定义了**缩写的名称**后，正文里只要遇到相同的字符，就会将其转换成带有缩写提示的 HTML 标签。



## 示例


```
The HTML specification
is maintained by the W3C.

*[W3C]:  World Wide Web Consortium
```

## 效果

The HTML specification
is maintained by the W3C.

*[W3C]:  World Wide Web Consortium


## HTML

``` html?fancy=2
<p>The HTML specification<br>
is maintained by the <abbr title=">>==World Wide Web Consortium==<<">W3C</abbr>.
</p>
```



# 疑问

缩写跟脚注不同的地方是，缩写不会在生成的正文结尾显示定义的内容，而脚注会在正文结尾显示用户定义的脚注内容。缩写定义会对正文里所有被定义的文字提供一个鼠标经过悬浮的提示。

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [php markdown extra](https://michelf.ca/projects/php-markdown/extra/#abbr)


[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-abbr