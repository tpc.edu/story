---
title: 小书匠语法说明之公式
tags: markdown, 语法,公式,小书匠
grammar_mathjax: true
slug: /grammar/advance/mathjax/
---

[toc]


# 概述

MathJax 是一个 JavaScript 库，是可以跨浏览器渲染数学公式的引擎。支持 LaTeX, MathML 和 AsciiMath 标记。

> MathJax is a cross-browser JavaScript library that displays mathematical notation in web browsers, using MathML, LaTeX and ASCIIMathML markup.


 <a href="https://www.mathjax.org">
    <img title="Powered by MathJax"
    src="https://www.mathjax.org/badge/badge-square.png"
    border="0" alt="Powered by MathJax" />
</a>


``` mathjax!
$$\def\iddots{{\kern3mu\raise1mu{.}\kern3mu\raise6mu{.}\kern3mu 
\raise12mu{.}}}$$
```
# 使用

元数据标识: **grammar_mathjax**

想要使用该语法，需要在 **设置>扩展语法** 里把**mathjax**选项打开。或者在每篇文章的元数据里通过 **grammar_mathjax** 进行控制。系统默认关闭**mathjax**语法功能


由于  mathjax 推荐的写法是 `$` 包裹，考虑到该符号在日常生活中有太多使用，如果使用该符号做为公式标识，容易造成文档解析错误，特别是在既有公式显示的需求，文档里又有大量普通 `$` 符号的内容时，对于用户很容易对公式的解析造成错误的判断。因此小书匠在公式标识的规则上做了特殊处理，只有在代码里加上感叹号后，才表示为要执行生成公式功能。

当然，小书匠也提供了一种兼容 mathjax 自己默认的写法，该选项需要用户自己到 **设置>扩展语法>mathjax** 里单独打开 **兼容常规写法** 功能。

## 行内公式

行内公式使用`` `!$ ``  和 `` `$ ``包裹，如果你在 `设置>扩展语法>mathjax` 里打开了`兼容通用写法` ，则可以直接把公式包裹在两个 `$` 符号之间

### 示例

```
`!$ \Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,. $`
```

### 效果

`!$ \Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,. $`

## 块级公式

块级公式在代码块里使用 mathjax 做为标识语言，并在结尾添加上一个感叹号，表示执行里面的代码。如果你在 `设置>扩展语法>mathjax` 里打开了`兼容通用写法` ，则可以直接把公式包裹在两个 `$$` 符号之间。

### 示例

````
```mathjax!
$$\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.$$
```
````

### 效果

```mathjax!
$$\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.$$
```

## 带编号的公式

公式编号可以跨代码块，也就是在其中的一个代码块里生成了一个编号，在其他代码块里可以引用该编号

### 示例

````?title=带编号的数学公式
`!$\eqref{ref1}$`

```mathjax!
\begin{equation}
\int_0^\infty \frac{x^22}{e^x-1}\,dx = \frac{\pi^4}{15}\label{ref1}
\end{equation}
```

`!$\eqref{ref1}$`
````

### 效果

`!$\eqref{ref1}$`

```mathjax!
\begin{equation}
\int_0^\infty \frac{x^22}{e^x-1}\,dx = \frac{\pi^4}{15}\label{ref1}
\end{equation}
```

```mathjax!
$$\eqref{ref1}$$
```

# Mathjax 常用符号

## 符号 (Operators)

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\pm$` | \pm | `!$\mp$` | \mp | `!$\times$` | \times |
| `!$\div$` | \div | `!$\cdot$` | \cdot | `!$\ast$` | \ast |
| `!$\star$` | \star | `!$\dagger$` | \dagger | `!$\ddagger$` | \ddagger |
| `!$\amalg$` | \amalg | `!$\cap$` | \cap | `!$\cup$` | \cup |
| `!$\uplus$` | \uplus | `!$\sqcap$` | \sqcap | `!$\sqcup$` | \sqcup |
| `!$\vee$` | \vee | `!$\wedge$` | \wedge | `!$\oplus$` | \oplus |
| `!$\ominus$` | \ominus | `!$\otimes$` | \otimes | `!$\circ$` | \circ |
| `!$\bullet$` | \bullet | `!$\diamond$` | \diamond | `!$\lhd$` | \lhd |
| `!$\rhd$` | \rhd | `!$\unlhd$` | \unlhd | [`!Unrhd.gif`](https://artofproblemsolving.com/wiki/index.php/File:Unrhd.gif) | \unrhd |
| `!$\oslash$` | \oslash | `!$\odot$` | \odot | `!$\bigcirc$` | \bigcirc |
| `!$\triangleleft$` | \triangleleft | `!$\Diamond$` | \Diamond | `!$\bigtriangleup$` | \bigtriangleup |
| `!$\bigtriangledown$` | \bigtriangledown | `!$\Box$` | \Box | `!$\triangleright$` | \triangleright |
| `!$\setminus$` | \setminus | `!$\wr$` | \wr | `!$\sqrt{x}$` | \sqrt{x} |
| `!$x^{\circ}$` | x^{\circ} | `!$\triangledown$` | \triangledown | `!$\sqrt[n]{x}$` | \sqrt[n]{x} |
| `!$a^x$` | a^x | `!$a^{xyz}$` | a^{xyz} |

## 关系(Relations)

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\le$` | \le | `!$\ge$` | \ge | `!$\neq$` | \neq |
| `!$\sim$` | \sim | `!$\ll$` | \ll | `!$\gg$` | \gg |
| `!$\doteq$` | \doteq | `!$\simeq$` | \simeq | `!$\subset$` | \subset |
| `!$\supset$` | \supset | `!$\approx$` | \approx | `!$\asymp$` | \asymp |
| `!$\subseteq$` | \subseteq | `!$\supseteq$` | \supseteq | `!$\cong$` | \cong |
| `!$\smile$` | \smile | `!$\sqsubset$` | \sqsubset | `!$\sqsupset$` | \sqsupset |
| `!$\equiv$` | \equiv | `!$\frown$` | \frown | `!$\sqsubseteq$` | \sqsubseteq |
| `!$\sqsupseteq$` | \sqsupseteq | `!$\propto$` | \propto | `!$\bowtie$` | \bowtie |
| `!$\in$` | \in | `!$\ni$` | \ni | `!$\prec$` | \prec |
| `!$\succ$` | \succ | `!$\vdash$` | \vdash | `!$\dashv$` | \dashv |
| `!$\preceq$` | \preceq | `!$\succeq$` | \succeq | `!$\models$` | \models |
| `!$\perp$` | \perp | `!$\parallel$` | \parallel |  |
| `!$\mid$` | \mid | `!$\bumpeq$` | \bumpeq |  |

上面这些关系符号的否定(反义)形式可以通过在原符号前添加 `\not` 来进行实现，或者在 `\` 和符号单词之间添加 `n` 来实现。下面列出几个常用的否定形式，其他符号的否定形式规则基本类似。

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\nmid$` | \nmid | `!$\nleq$` | \nleq | `!$\ngeq$` | \ngeq |
| `!$\nsim$` | \nsim | `!$\ncong$` | \ncong | `!$\nparallel$` | \nparallel |
| `!$\not<$` | \not< | `!$\not>$` | \not> | `!$\not=$` | \not= |
| `!$\not\le$` | \not\le | `!$\not\ge$` | \not\ge | `!$\not\sim$` | \not\sim |
| `!$\not \approx$` | \not\approx | `!$\not\cong$` | \not\cong | `!$\not\equiv$` | \not\equiv |
| `!$\not\parallel$` | \not\parallel | `!$\nless$` | \nless | `!$\ngtr$` | \ngtr |
| `!$\lneq$` | \lneq | `!$\gneq$` | \gneq | `!$\lnsim$` | \lnsim |
| `!$\lneqq$` | \lneqq | `!$\gneqq$` | \gneqq |

像 `=`, `>`, and `<` 这些没有列在上面的符号，可以直接字面输入，并不需要命令进行触发。

## 希腊字母(Greek Letters)


| Symbol | Command | Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- | -- | -- |
| `!$\alpha$` | \alpha | `!$\beta$` | \beta | `!$\gamma$` | \gamma | `!$\delta$` | \delta |
| `!$\epsilon$` | \epsilon | `!$\varepsilon$` | \varepsilon | `!$\zeta$` | \zeta | `!$\eta$` | \eta |
| `!$\theta$` | \theta | `!$\vartheta$` | \vartheta | `!$\iota$` | \iota | `!$\kappa$` | \kappa |
| `!$\lambda$` | \lambda | `!$\mu$` | \mu | `!$\nu$` | \nu | `!$\xi$` | \xi |
| `!$\pi$` | \pi | `!$\varpi$` | \varpi | `!$\rho$` | \rho | `!$\varrho$` | \varrho |
| `!$\sigma$` | \sigma | `!$\varsigma$` | \varsigma | `!$\tau$` | \tau | `!$\upsilon$` | \upsilon |
| `!$\phi$` | \phi | `!$\varphi$` | \varphi | `!$\chi$` | \chi | `!$\psi$` | \psi |
| `!$\omega$` | \omega |


| Symbol | Command | Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- | -- | -- |
| `!$\Gamma$` | \Gamma | `!$\Delta$` | \Delta | `!$\Theta$` | \Theta | `!$\Lambda$` | \Lambda |
| `!$\Xi$` | \Xi | `!$\Pi$` | \Pi | `!$\Sigma$` | \Sigma | `!$\Upsilon$` | \Upsilon |
| `!$\Phi$` | \Phi | `!$\Psi$` | \Psi | `!$\Omega$` | \Omega |

## 箭头(Arrows)

| Symbol | Command | Symbol | Command |
| -- | -- | -- | -- |
| `!$\gets$` | \gets | `!$\to$` | \to |
| `!$\leftarrow$` | \leftarrow | `!$\Leftarrow$` | \Leftarrow |
| `!$\rightarrow$` | \rightarrow | `!$\Rightarrow$` | \Rightarrow |
| `!$\leftrightarrow$` | \leftrightarrow | `!$\Leftrightarrow$` | \Leftrightarrow |
| `!$\mapsto$` | \mapsto | `!$\hookleftarrow$` | \hookleftarrow |
| `!$\leftharpoonup$` | \leftharpoonup | `!$\leftharpoondown$` | \leftharpoondown |
| `!$\rightleftharpoons$` | \rightleftharpoons | `!$\longleftarrow$` | \longleftarrow |
| `!$\Longleftarrow$` | \Longleftarrow | `!$\longrightarrow$` | \longrightarrow |
| `!$\Longrightarrow$` | \Longrightarrow | `!$\longleftrightarrow$` | \longleftrightarrow |
| `!$\Longleftrightarrow$` | \Longleftrightarrow | `!$\longmapsto$` | \longmapsto |
| `!$\hookrightarrow$` | \hookrightarrow | `!$\rightharpoonup$` | \rightharpoonup |
| `!$\rightharpoondown$` | \rightharpoondown | `!$\leadsto$` | \leadsto |
| `!$\uparrow$` | \uparrow | `!$\Uparrow$` | \Uparrow |
| `!$\downarrow$` | \downarrow | `!$\Downarrow$` | \Downarrow |
| `!$\updownarrow$` | \updownarrow | `!$\Updownarrow$` | \Updownarrow |
| `!$\nearrow$` | \nearrow | `!$\searrow$` | \searrow |
| `!$\swarrow$` | \swarrow | `!$\nwarrow$` | \nwarrow |

(有些箭头指令, mathjax 提供了缩写指令, `\iff` 和 `\implies`  可以分别表示为 `\Longleftrightarrow` 和 `\Longrightarrow` 。)

## 点(Dots)

| Symbol | Command | Symbol | Command |
| -- | -- | -- | -- |
| `!$\cdot$` | \cdot | `!$\vdots$` | \vdots |
| `!$\dots$` | \dots | `!$\ddots$` | \ddots |
| `!$\cdots$` | \cdots | `!$\iddots$` | \iddots |

## 重音(Accents)

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\hat{x}$` | \hat{x} | `!$\check{x}$` | \check{x} | `!$\dot{x}$` | \dot{x} |
| `!$\breve{x}$` | \breve{x} | `!$\acute{x}$` | \acute{x} | `!$\ddot{x}$` | \ddot{x} |
| `!$\grave{x}$` | \grave{x} | `!$\tilde{x}$` | \tilde{x} | `!$\mathring{x}$` | \mathring{x} |
| `!$\bar{x}$` | \bar{x} | `!$\vec{x}$` | \vec{x} |

当重音修饰在像 `i` 和 `j` 这两个字母上时，你可以分别通过 `\imath` 和 `\jmath` ，把它们头上的点去掉。

| Symbol | Command | Symbol | Command |
| -- | -- | -- | -- |
| `!$\vec{\jmath}$` | \vec{\jmath} | `!$\tilde{\imath}$` | \tilde{\imath} |

`\tilde` 和 `\hat` 两个指令有宽符号的版本 `\widetilde` 和 `\widehat`，通过这两个指令可以生成长版本的表达式结构的符号.

| Symbol | Command | Symbol | Command |
| -- | -- | -- | -- |
| `!$\widehat{7+x}$` | \widehat{7+x} | `!$\widetilde{abc}$` | \widetilde{abc} |

## 其他(Others)

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\infty$` | \infty | `!$\triangle$` | \triangle | `!$\angle$` | \angle |
| `!$\aleph$` | \aleph | `!$\hbar$` | \hbar | `!$\imath$` | \imath |
| `!$\jmath$` | \jmath | `!$\ell$` | \ell | `!$\wp$` | \wp |
| `!$\Re$` | \Re | `!$\Im$` | \Im | `!$\mho$` | \mho |
| `!$\prime$` | \prime | `!$\emptyset$` | \emptyset | `!$\nabla$` | \nabla |
| `!$\surd$` | \surd | `!$\partial$` | \partial | `!$\top$` | \top |
| `!$\bot$` | \bot | `!$\vdash$` | \vdash | `!$\dashv$` | \dashv |
| `!$\forall$` | \forall | `!$\exists$` | \exists | `!$\neg$` | \neg |
| `!$\flat$` | \flat | `!$\natural$` | \natural | `!$\sharp$` | \sharp |
| `!$\backslash$` | \backslash | `!$\Box$` | \Box | `!$\Diamond$` | \Diamond |
| `!$\clubsuit$` | \clubsuit | `!$\diamondsuit$` | \diamondsuit | `!$\heartsuit$` | \heartsuit |
|`!$\spadesuit$` | \spadesuit | `!$\Join$` | \Join | `!$\blacksquare$` | \blacksquare |
| `!$\S$` | \S |  |  |  |  |
| `!$\bigstar$` | \bigstar | `!$\in$` | \in | `!$\cup$` | \cup |
| `!$\square$` | \square |  |
| `!$\mathbb{R}$` | \mathbb{R} (represents all real numbers) |  |
| `!$\checkmark$` | \checkmark |  |

## 命令符号(Command Symbols)

有些符号是有特殊意义，比如用在指令上，如果想显示这些符号，就需要进行一些特殊处理。大部份通过 `\` 符号对其进行转义，也有些是通过符号的单词名称进行显示。

| Symbol | Command | Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- | -- | -- |
| `!$\$$` | $ | `!$\&$` | \& | `!$\%$` | \% | `!$\#$` | \# |
| `!$\_$` | \_ | `!$\{$` | \{ | `!$\}$` | \} | `!$\backslash$` | \backslash |


## 界线符号(Bracketing Symbols)

In mathematics, sometimes we need to enclose expressions in brackets or braces or parentheses. Some of these work just as you'd imagine in LaTeX; type ( and ) for parentheses, [ and ] for brackets, and | and | for absolute value. However, other symbols have special commands:
在数学公式里，有时我们会通过括号(`()`, `[]`, `{}`)进行界线控制。这些符号有些是可以直接输入，比如 `()`, `[]`, `|` 等，而有些符号是要经过转义的，下面列出了这些比较特殊的符号

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\{$` | \{ | `!$\}$` | \} | `!$\|$` | \| |
| `!$\backslash$` | \backslash | `!$\lfloor$` | \lfloor | `!$\rfloor$` | \rfloor |
| `!$\lceil$` | \lceil | `!$\rceil$` | \rceil | `!$\langle$` | \langle |
| `!$\rangle$` | \rangle |


当界线符号用在像下面的表达式时

```
(\frac{a}{x} )^2
```

会发现界线符号没有足够高

 ``` mathjax!
 $$(\frac{a}{x})^2$$
 ```

通过输入 `\left` 和 `\right` 在相关的界线符号前面时，界线符号生成的效果就会比较理想

```
\left(\frac{a}{x} \right)^2
```

这是应用了 `\left` 和 `\right` 的最终效果

```mathjax!
$$\left(\frac{a}{x} \right)^2$$
```

另一个应用 `\left` 和 `\right` 的例子
```
\left\{\begin{array}{l}x+y=3\\2x+y=5\end{array}\right.
```

显示结果

```mathjax!
$$\left\{\begin{array}{l}x+y=3\\2x+y=5\end{array}\right.$$
```

See that there's a dot after `\right`. You must put that dot or the code won't work.

注意在 `\right` 后面有一个点 `.`。如果不输入这个点的话，该公式将会无效

`\underbrace` 可以显示下括号

```
\underbrace{a_0+a_1+a_2+\cdots+a_n}_{x}
```

显示效果

```mathjax!
$$\underbrace{a_0+a_1+a_2+\cdots+a_n}_{x}$$
```

`\overbrace` 可以显示上括号

```
\overbrace{a_0+a_1+a_2+\cdots+a_n}^{x}
```

显示效果

```mathjax!
$$\overbrace{a_0+a_1+a_2+\cdots+a_n}^{x}$$
```

`\left` and `\right` 也可以用来改变下面这些符号的大小

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\uparrow$` | \uparrow | `!$\downarrow$` | \downarrow | `!$\updownarrow$` | \updownarrow |
| `!$\Uparrow$` | \Uparrow | `!$\Downarrow$` | \Downarrow | `!$\Updownarrow$` | \Updownarrow |

## 不同尺寸的符号

有些符号在行模式和块模式的渲染效果会不一样。所谓的行模式就是使用了 `$` 符号包裹的公式，而块模式就是类似使用 `$$` 包裹的公式。

下面的列表里显示了一些符号在行模式和块模式的差别

| Symbol | Command | Symbol | Command | Symbol | Command |
| -- | -- | -- | -- | -- | -- |
| `!$\sum$` $$\sum$$ | \sum | `!$\int$` $$\int$$ | \int | `!$\oint$` $$\oint$$ | \oint |
| `!$\prod$` $$\prod$$ | \prod | `!$\coprod$` $$\coprod$$ | \coprod | `!$\bigcap  $` $$\bigcap$$| \bigcap |
| `!$\bigcup$` $$\bigcup$$ | \bigcup | `!$\bigsqcup$` $$\bigsqcup$$ | \bigsqcup | `!$\bigvee$` $$\bigvee$$ | \bigvee |
| `!$\bigwedge$` $$\bigwedge$$ | \bigwedge | `!$\bigodot$` $$\bigodot$$ | \bigodot | `!$\bigotimes$` $$\bigotimes$$ | \bigotimes |
| `!$\bigoplus$` $$\bigoplus$$ | \bigoplus | `!$\biguplus$` $$\biguplus$$| \biguplus |



# 疑问



# 相关

1. [mathjax 官方网站](https://www.mathjax.org/)
2. [mathjax 支持的 text 命令](http://docs.mathjax.org/en/latest/tex.html#tex-commands)
3. [更多 mathjax 符号](http://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)
4. [latex 相关](https://artofproblemsolving.com/wiki/index.php?title=LaTeX:Symbols)