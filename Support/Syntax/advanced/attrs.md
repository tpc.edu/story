---
title: 小书匠语法说明之html属性配置attrs
tags: markdown, 语法,html属性,小书匠
slug: /grammar/advance/attrs/
grammar_attrs: true
grammar_decorate: false
---

[toc]

```python {data=asdf}
nums = [x for x in range(10)]
```


# 概述

html属性配置(attrs) 语法属于修饰性语法，可以对生成的 html 片段添加属性 ，比如给 html 标签添加 class, id, style 等属性，方便用户对该 html 片段进行个性化的定义。使用该语法需要用户对 html, css 有一定的基础。

该语法在不同的编辑器或者解析器支持不一样，小书匠使用  [markdown-it][1] 的扩展 [markdown-it-attrs][2] 实现对html属性修改的支持。

# 使用

元数据标识: **grammar_attrs**

想要使用该语法，需要在**设置>扩展语法** 里把 **html属性配置(attrs)** 选项打开。或者在每篇文章的元数据里通过 **grammar_attrs** 进行控制。系统默认关闭 **html属性配置(attrs)** 语法功能

该语法与 [decorate 语法][3] 和 [class 语法][4] 冲突，使用 **html属性配置(attrs)** 时，需要禁用 **decorate 语法** 和 **class 语法**语法

只要在想要特殊处理的块级语法或者行内语法结尾添加以符号 `{` 和 `}` 包裹的内容即可。被包裹的内容里 class 名称可以以 `.` 符号开头, id名称可以以 `#` 符号开头， 其他 html 属性，以键值对的方式填写，键值对之间以 `=` 符号做标记，多个键值对以空格区分

详细使用规则也可以参考这里 https://github.com/arve0/markdown-it-attrs

## 示例

```
## 被添加了多种属性的二号标题头 {.class_name #id_name data-message="hello world"}
```

## 效果

## 被添加了多种属性的二号标题头 {.class_name  #id_name data-message="hello world"}

## HTML 片段

``` html?linenums
<h2 class="class_name" id="id_name" data-message="hello world"><span class="xsj_heading_content">被添加了多种属性的二号标题头</span></h2>
```

# 疑问

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [decorate 语法][3]
4. [classy 语法][4]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/rstacruz/markdown-it-attrs
[3]: /grammar/advance/decorate/
[4]: /grammar/advance/class/