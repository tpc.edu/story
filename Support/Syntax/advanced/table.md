---
title:  小书匠语法说明之表格
tags: markdown, 语法,表格,小书匠
slug: /grammar/advance/table/
grammar_tableExtra: true
grammar_code: true
---

[toc]


# 概述

小书匠除了支持基础的表格语法外，还对表格功能进行能增强，比如行合并单元格，列合并单元格，多表格头，多表格身等

# 普通表格


## 常规用法

### 示例

```
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

### 效果

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

## 表格内容对齐

### 示例

单元格左对齐

```?title=单元格左对齐&fancy=2
First Header  | Second Header
:------------ | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

```?title=单元格右对齐&fancy=2
First Header  | Second Header
------------: | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

```?title=单元格居中对齐&fancy=2
First Header  | Second Header
:-----------: | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

### 效果

左对齐

First Header  | Second Header
:------------ | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell


右对齐

First Header  | Second Header
------------: | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

居中对齐

First Header  | Second Header
:-----------: | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

# 增强型表格

## 如何打开增强型表格语法功能


元数据 : **grammar_tableExtra**

* 通过 **小书匠主按钮>设置>扩展语法>增强型表格** 进行全局控制
* 通过每篇文章元数据进行开关控制 **grammar_tableExtra**

## 行单元格合并

如果单元格的右侧内容为空时，系统自动向右合并单元格。

系统也支持在前一个单元格输入 `>` ，来把当前的单元格合并到下一个单元格里。

### 示例

```?fancy=3，10
First Header  | Second Header
------------- | -------------
行合并的单元格  >>==| ==<<
Content Cell  | Content Cell

另一种方式

First Header  | Second Header
------------- | -------------
>>==>==<<  | 行合并的单元格
Content Cell  | Content Cell

```



### 效果

First Header  | Second Header
------------- | -------------
行合并的单元格 | 
Content Cell  | Content Cell

方式二

First Header  | Second Header
------------- | -------------
>  | 行合并的单元格
Content Cell  | Content Cell

### HTML

```html?fancy=14
<table class="table-extra table table-striped table-celled">
<colgroup>
<col>
<col>
</colgroup>
<thead>
<tr>
<th>First Header</th>
<th>Second Header</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2">行合并的单元格</td>

</tr>
<tr>
<td>Content Cell</td>
<td>Content Cell</td>
</tr>
</tbody>
</table>
```

## 列单元格合并

在想要被合并的单元格添加 `^` 符号。

### 示例

```?fancy=4
First Header  | Second Header
------------- | -------------
>>==合并的列单元格==<<  | Content Cell
>>==^==<< | Content Cell
```

### 效果

First Header  | Second Header
------------- | -------------
合并的列单元格  | Content Cell
^ | Content Cell

### HTML

``` html?fancy=14
<table class="table-extra table table-striped table-celled">
<colgroup>
<col>
<col>
</colgroup>
<thead>
<tr>
<th>First Header</th>
<th>Second Header</th>
</tr>
</thead>
<tbody>
<tr>
<td rowspan="2">合并的列单元格</td>
<td>Content Cell</td>
</tr>
<tr>

<td>Content Cell</td>
</tr>
</tbody>
</table>
```

## 表格标题

在表格定义的结尾添加 `[标题内容]`
### 示例

```?fancy=5
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
[表格标题内容]
```

### 效果

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
[表格标题内容]

### HTML

``` html?fancy=2
<table class="table-extra table table-striped table-celled">
<caption>表格标题内容</caption>
<colgroup>
<col>
<col>
</colgroup>
<thead>
<tr>
<th>First Header</th>
<th>Second Header</th>
</tr>
</thead>
<tbody>
<tr>
<td>Content Cell</td>
<td>Content Cell</td>
</tr>
<tr>
<td>Content Cell</td>
<td>Content Cell</td>
</tr>
</tbody>
</table>
```

## 多表格头

普通表格只能在 `----` 分界线的上方添加一个表格头，增强版的表格允许添加多个表格头

### 示例

```?fancy=1,2
表格头1  | 表格头1
表格头2  | 表格头2
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```

### 效果

表格头1  | 表格头1
表格头2  | 表格头2
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

### HTML

``` html?fancy=6-15
<table class="table-extra table table-striped table-celled">
<colgroup>
<col>
<col>
</colgroup>
<thead>
<tr>
<th>表格头1</th>
<th>表格头1</th>
</tr>
<tr>
<th>表格头2</th>
<th>表格头2</th>
</tr>
</thead>
<tbody>
<tr>
<td>Content Cell</td>
<td>Content Cell</td>
</tr>
<tr>
<td>Content Cell</td>
<td>Content Cell</td>
</tr>
</tbody>
</table>
```

## 多表格身

当表格内容部份有空行时，系统会生成多表格身效果。在 html 上就是生成多个 tbody

### 示例

```?fancy=5
First Header  | Second Header
------------- | -------------
表格内容1  | 表格内容1
表格内容1  | 表格内容1

表格内容2  | 表格内容2
表格内容2  | 表格内容2
```

### 效果

First Header  | Second Header
------------- | -------------
表格内容1  | 表格内容1
表格内容1  | 表格内容1

表格内容2  | 表格内容2
表格内容2  | 表格内容2

### HTML

```html?fancy=12,21,22,31
<table class="table-extra table table-striped table-celled">
<colgroup>
<col>
<col>
</colgroup>
<thead>
<tr>
<th>First Header</th>
<th>Second Header</th>
</tr>
</thead>
<tbody>
<tr>
<td>表格内容1</td>
<td>表格内容1</td>
</tr>
<tr>
<td>表格内容1</td>
<td>表格内容1</td>
</tr>
</tbody>
<tbody>
<tr>
<td>表格内容2</td>
<td>表格内容2</td>
</tr>
<tr>
<td>表格内容2</td>
<td>表格内容2</td>
</tr>
</tbody>
</table>
```

## 一个比较复杂的表格例子

### 示例

```
|First Header  | Second Header ||
|First Header  | Second Header | Third Header|
|------------- | -------------|-------------|
表身1Content Cell  | Merge Content Cell||
Content Cell  | Content Cell| Content Cell|

表身2Content Cell  | Merge Content Cell||
Content Cell  | Content Cell| Content Cell|
[表格标题]
```

### 效果

|First Header  | Second Header ||
|First Header  | Second Header | Third Header|
|------------- | -------------|-------------|
表身1Content Cell  | Merge Content Cell||
Content Cell  | Content Cell| Content Cell|

表身2Content Cell  | Merge Content Cell||
Content Cell  | Content Cell| Content Cell|
[表格标题]

# 疑问

# 相关

表格组件