---
title: 小书匠语法说明之序列图
tags: markdown, 语法,序列图,小书匠
grammar_sequence: true
slug: /grammar/advance/sequence/
---

[toc]


# 概述

序列图使用 [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/) 插件实现。

# 使用

元数据: **grammar_sequence**

想要使用该语法，需要在**设置>扩展语法** 里把**序列图**选项打开。或者在每篇文章的元数据里通过 **grammar_sequence** 进行控制。

该语法支持两种主题， **simple** 和 **hand** 。 可以通过全局设置里进行设置，也可以在每个代码块的查询参数里控制。系统默认使用 **hand** 主题。


## 示例

````
```sequence!
小明->小李: 你好 小李, 最近怎么样?
Note right of 小李: 小李想了想
小李-->小明: 还是老样子
```
````

使用 **simple** 主题的序列图

````
```sequence!?theme=simple
小明->小李: 你好 小李, 最近怎么样?
Note right of 小李: 小李想了想
小李-->小明: 还是老样子
```
````

## 效果

```sequence!
小明->小李: 你好 小李, 最近怎么样?
Note right of 小李: 小李想了想
小李-->小明: 还是老样子
```

使用 **simple** 主题的序列图

```sequence!?theme=simple
小明->小李: 你好 小李, 最近怎么样?
Note right of 小李: 小李想了想
小李-->小明: 还是老样子
```

# 序列图

## 语法

![序列语法图](./images/1546006321068.png)

## 序列图提供的几种连线

````
``` sequence!
Title: 几种不同风格的连线
A->B: 实体箭头实体线
B-->C: 实体箭头虚线
C->>D: 空心箭头实体线
D-->>A: 空心箭头虚线
```
````

``` sequence!
Title: 几种不同风格的连线
A->B: 实体箭头实体线
B-->C: 实体箭头虚线
C->>D: 空心箭头实体线
D-->>A: 空心箭头虚线
```

## 标注的几种不同位置

````
``` sequence!
Note left of A: 居左标注
Note right of A: 居右标注
Note over A: 横跨单个结点的标注
Note over A,B: 横跨两个结点的标注
Note over A,B,C: 支持换行的标注
```
````

``` sequence!
Note left of A: 居左标注
Note right of A: 居右标注
Note over A: 横跨单个结点的标注
Note over A,B: 横跨两个结点的标注
Note over B,C: 标注内容\n折行
```

## 修改参考者的顺序

````?fancy=2,3,4
``` sequence!
participant C
participant B
participant A
Note right of A: 通过对 participants 的顺序调整\n 改变序列图显示的顺序
```
````

``` sequence!
participant C
participant B
participant A
Note right of A: 通过对 participants 的顺序调整\n 改变序列图显示的顺序
```


# 疑问

# 相关

1. [js-sequence-diagrams 序列图官网](https://bramp.github.io/js-sequence-diagrams/)
2. https://github.com/bramp/js-sequence-diagrams/blob/master/src/grammar.jison