---
title: 小书匠语法说明之流程图
tags: markdown, 语法,流程图,小书匠
slug: /grammar/advance/flow/
grammar_flow: true
---

[toc]


# 概述

流程图使用 [flowchartjs](https://github.com/adrai/flowchart.js) 插件实现

# 使用

元数据: **grammar_flow**

想要使用该语法，需要在`设置>扩展语法` 里把`流程图`选项打开。或者在每篇文章的元数据里通过 `grammar_flow` 进行控制


## 示例

````
```flow
st=>start: 开始
e=>end: 结束
op=>operation: 操作步骤
cond=>condition: 是 或者 否?

st->op->cond
cond(yes)->e
cond(no)->op
```
````

## 效果

```flow
st=>start: 开始
e=>end: 结束
op=>operation: 操作步骤
cond=>condition: 是 或者 否?

st->op->cond
cond(yes)->e
cond(no)->op
```


``` flow!
st=>start: Start|past:>http://www.google.com[blank]
e=>end: End|future:>http://www.google.com
op1=>operation: My Operation|past
op2=>operation: Stuff|current
sub1=>subroutine: My Subroutine|invalid
cond=>condition: Yes
or No?|approved:>http://www.google.com
c2=>condition: Good idea|rejected
io=>inputoutput: catch something...|future

st->op1(right)->cond
cond(yes, right)->c2
cond(no)->sub1(left)->op1
c2(yes)->io->e
c2(no)->op2->e
```

# 流程图

流程图语法主要分为两部份，一个是结点定义，一个是流程定义。结点定义设置各个结点类型，及显示文字。流程定义是使用结点定义时定义的各结点

```
结点定义
结点名称=>结点类型:结点说明

流程定义
结点名称->结点名称
```

## 不同的结点

主要结点有 

| 类型        | 名称         |
| ----------- | ------------ |
| start       | 起始结点     |
| end         | 结束结点     |
| operation   | 处理结点     |
| subroutine  | 子路由结点   |
| condition   | 条件结点     |
| inputoutput | 输入输出结点 |
| parallel    | 并行结点     |

示例

````
```flow!
st=>start: Start:>http://www.google.com[blank]
e=>end:>http://www.google.com
op1=>operation: My Operation
sub1=>subroutine: My Subroutine
cond=>condition: Yes
or No?:>http://www.google.com
io=>inputoutput: catch something...
para=>parallel: parallel tasks

st->op1->cond
cond(yes)->io->e
cond(no)->para
para(path1, bottom)->sub1(right)->op1
para(path2, top)->op1
```
````

```flow!
st=>start: Start:>http://www.google.com[blank]
e=>end:>http://www.google.com
op1=>operation: My Operation
sub1=>subroutine: My Subroutine
cond=>condition: Yes
or No?:>http://www.google.com
io=>inputoutput: catch something...
para=>parallel: parallel tasks

st->op1->cond
cond(yes)->io->e
cond(no)->para
para(path1, bottom)->sub1(right)->op1
para(path2, top)->op1
```

## 可点击的结点

在结点定义阶段，想要可点击的结点结尾添加 `:>` ，并带上要跳转的地址。

```
结点定义
结点名称=>结点类型:结点说明:>链接地址
```
默认是在本页进行跳转，可以在链接地址后面添加 `[blank]`, 用来在新窗口打开

```
结点定义
结点名称=>结点类型:结点说明:>链接地址[blank]
```

## 条件结点

定义为条件结点后，在流程定义阶段，可以对条件结点进行 `yes` 和 `no` 参数的传递

```
流程定义
条件结点名称(yes)->结点名称
条件结点名称(no)->结点名称
```

## 并行结点

在流程定义阶段，可以对并行结点进行不同的参数设置



# 疑问

# 相关

1. [flowchart 官网](https://github.com/adrai/flowchart.js)