---
title: 小书匠语法说明之下标
tags: markdown, 语法,下标,小书匠
slug: /grammar/advance/subscript/
---

[toc]


# 概述

下标语法不是标准的 commonmark， 不同的编辑器或者博客平台支持可能不一样。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-sub][2] 实现对下标的支持。


# 使用

元数据标识: **grammar_sub**

想要使用该语法，需要在`设置>扩展语法` 里把**下角文字**选项打开。或者在每篇文章的元数据里通过 `grammar_sub` 进行控制。系统默认打开**下角文字**语法功能

然后将想要进行下标显示的文字包裹在 `~` 符号内就可以。

## 示例

```
下角文字: H~2~O
```

## 效果

下角文字: H~2~O

## html

```
H<sub>2</sub>O
```

# 注

github 没有提供对下标的支持，它们会将 `～`解析成删除线

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [markdown-it-sub 扩展插件][2]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-sub