---
title: 小书匠语法说明之自定义class
tags: markdown, 语法,自定义class,小书匠
slug: /grammar/advance/class/
grammar_classy: true
grammar_attrs: false
grammar_decorate: false
---

[toc]


# 概述

class语法属于修饰性语法，主要功能就是给生成的 html 添加 class 属性，方便用户对该 html 片段进行个性化的定义。使用该语法需要用户对 html, css 有一定的基础。

该语法在不同的编辑器或者解析器支持不一样，小书匠使用  [markdown-it][1] 的扩展 [markdown-it-classy][2] 实现对html属性修改的支持。

# 使用

元数据标识: **grammar_classy**

想要使用该语法，需要在**设置>扩展语法** 里把 **自定义 class** 选项打开。或者在每篇文章的元数据里通过 **grammar_classy** 进行控制。系统默认关闭 **自定义 class** 语法功能

该语法与 [decorate语法][3] 和 [attrs 语法][4]  冲突，使用 **自定义 class** 时，需要禁用 **decorate 语法** 和 **attrs 语法**

只要在想要特殊处理的块级语法或者行内语法结尾添加以符号 `{` 和 `}` 包裹的样式名称即可。

## 示例

对标题添加自定义的 class
```
## 添加了自定义的 class {custom_class}
```

## 效果

## 添加了自定义的 class {custom_class}


## HTML 片段

``` html
<h2 class="custom_class"><span class="xsj_heading_content">添加了自定义的 class</span></h2>
```
# 疑问

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [decorate 语法][3]
4. [attrs 语法][4]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/andrey-p/markdown-it-classy
[3]: /grammar/advance/decorate/
[4]: /grammar/advance/attrs/