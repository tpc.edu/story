---
title: 小书匠语法说明之中文斜体
tags: markdown, 语法,cjk强调,小书匠
slug: /grammar/feature/cjk_emphasis/
grammar_cjkEmphasis: true
---

[toc]


# 概述

cjk强调语法用于对中文强调功能。该语法并非 commonmark 标准语法，目前只有小书匠提供了支持。

# 使用

元数据标识: grammar_cjkEmphasis

提供针对CJK强调格式显示，由于中文并没有真正意义上的斜体做为强调，该语法自动将斜体转换成对应的衬线字体．目前只设计了中文字体对应的衬线字体。

## 示例

````
_需要显示中文衬线字体的内容_
````

## 效果

_需要显示中文衬线字体的内容_

# 疑问

# 相关