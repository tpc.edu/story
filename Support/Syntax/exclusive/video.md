---
title: 小书匠语法说明之视频
tags: markdown, 语法,视频,小书匠
slug: /grammar/feature/video/
grammar_video: true
---

[toc]


# 概述

视频语法用于在文章内嵌入一段视频内容，视频内容支持网络地址的视频，也支持本地上传到小书匠内置数据库里的视频。

该语法并非 commonmark 标准语法，目前只有小书匠提供了支持。

# 使用

元数据标识: **grammar_video**

想要使用该语法，需要在**设置>扩展语法** 里把**视频**选项打开。或者在每篇文章的元数据里通过 **grammar_video** 进行控制。系统默认打开**视频**语法功能

视频语法书写格式主要借鉴了图片语法的书写格式，都提供了内联式写法和引用式写法，不同的是视频语法使用的是 `%` 符号代替图片语法的 `!` 符号。

## 内联式视频

### 示例

```
%[电影](http://markdown.xiaoshujiang.com/media/movie.ogg)
```

### 效果

%[电影](http://markdown.xiaoshujiang.com/media/movie.ogg)

## 引用式视频

### 示例

```
%[电影][a]

[a]: http://markdown.xiaoshujiang.com/media/movie.ogg
```

### 效果

%[电影][a]

[a]: http://markdown.xiaoshujiang.com/media/movie.ogg


# 注

需要注意的是，不建议将太大的视频文件保存到小书匠内置数据库里，而是自己上传到一个专用的视频服务器，然后再用该视频地址

# 相关