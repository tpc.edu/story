---
title: 小书匠 wiki 链接语法
tags: Markdown,wiki, 链接,小书匠,语法
slug: /grammar/feature/wiki_link/
grammar_wikilink: true
---

[toc]

# 概述

wiki 链接语法用于识别 wiki 格式的链接 `[[链接地址|显示文字]]`.



# 使用

元数据标识: grammar_wikilink

想要使用该语法，需要在 **设置>扩展语法** 里把 **wiki 链接** 选项打开。或者在每篇文章的元数据里通过 **grammar_wikilink** 进行控制。系统默认关闭 wiki 链接语法功能。


# 示例

```
[[http://www.xiaoshujiang.com]]

[[http://www.xiaoshujiang.com|小书匠官方网站]]
```


# 效果

[[http://www.xiaoshujiang.com]]

[[http://www.xiaoshujiang.com|小书匠官方网站]]