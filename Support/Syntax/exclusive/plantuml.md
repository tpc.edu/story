---
title: 小书匠语法说明之plantuml
tags: markdown, 语法,plantuml,小书匠
slug: /grammar/feature/plantuml/
grammar_plantuml: true
---

[toc]


# 概述

plantuml是一个非常方便的uml工具，只要简单的编写文本，就可以得到对应的UML图形。这样的功能，对于写技术类的文章，特别有帮助。再加上自身是一种纯文本的格式，更加方便了文件版本的管理。plantuml除了可以绘制标准的uml图之外，还能绘制界面布局图、结构图、甘特图乃至于数学公式等。可谓“plantuml在手，天下我有”。

plantuml 语法不是标准的 commonmark语法，不同编辑器或者博客对其支持的程度不一样， 目前小书匠提供了对该语法的完整支持，并且实现了实时预览的效果。

> PlantUML is an open-source tool allowing users to create UML diagrams from a plain text language. The language of PlantUML is an example of an Application Specific Language.[3] It uses Graphviz software to lay out its diagrams. It has been used to allow blind students to work with UML.[4][5] PlantUML also helps blind software engineers to design and read UML diagrams.

# 使用

元数据标识: **grammar_plantuml**

想要使用该语法，需要在 **设置>扩展语法** 里把**plantuml**选项打开。或者在每篇文章的元数据里通过 **grammar_plantuml** 进行控制。系统默认关闭**plantuml**语法功能

更详细的语法使用，可以参考这里 http://plantuml.com/

plantuml 语法需要使用第三方服务器，系统默认指定小书匠自定义的服务器，内网用户可以通过 **设置>扩展语法>plantuml** 里的服务器选项，修改成自己的 plantuml 服务器地址

代码段内的文字将被encode成base64并被压缩后做为 :uml 内容提交到服务器。

## 示例

````
```plantuml!
  Bob->Alice : hello
  ```
````

## 效果

```plantuml!
  Bob->Alice : hello
  ```
  
# plantuml 

## 基础

### 普通

````
``` plantuml!
title Title

( )
note left : Note

[  ]
note right : Note

' single-line comment

/'
  block comment
'/
```
````


``` plantuml!
title Title

( )
note left : Note

[  ]
note right : Note

' single-line comment

/'
  block comment
'/
```

### 参与者

````
``` plantuml!
actor Actor
boundary Boundary
control Control
entity Entity
database Database
:Actor alias:
```
````


  ``` plantuml!
actor Actor
boundary Boundary
control Control
entity Entity
database Database
:Actor alias:
  ```
  

### 箭头

````
``` plantuml!
up -up-> right
-right-> down
-down-> left
-left-> up
```
````

``` plantuml!
up -up-> right
-right-> down
-down-> left
-left-> up
```

### 颜色

````
``` plantuml!
' =================
' == Declaration ==
' =================

[Component 1]

node "Node 1" {
    package "Package 1" #Orange {
        [Component 4]
        [Component 3]
    }
    [Component 2]
}



' ====================
' == Implementation ==
' ====================


node "Node 1" {
    [Component 2] .[#Green]-> [Component 4]
    [Component 3] <-left-> [Component 4]
    [Component 4] -- [Component 1]
}
```
````

``` plantuml!
' =================
' == Declaration ==
' =================

[Component 1]

node "Node 1" {
    package "Package 1" #Orange {
        [Component 4]
        [Component 3]
    }
    [Component 2]
}



' ====================
' == Implementation ==
' ====================


node "Node 1" {
    [Component 2] .[#Green]-> [Component 4]
    [Component 3] <-left-> [Component 4]
    [Component 4] -- [Component 1]
}
```



## 常见 UML 图

### 用例图

````
``` plantuml!
actor Promoter
actor Entrant

Promoter --> (Create Event)
Promoter -> (Attend Event)

Entrant --> (Find Event)
(Attend Event) <- Entrant

(Attend Event) <.. (Create Member)  : <<include>>
```
````

``` plantuml!
actor Promoter
actor Entrant

Promoter --> (Create Event)
Promoter -> (Attend Event)

Entrant --> (Find Event)
(Attend Event) <- Entrant

(Attend Event) <.. (Create Member)  : <<include>>
```

### 活动图

````
``` plantuml!
(*) --> "Find Event"
"Find Event" -> "Attend Event"

if "Capacity?" then
  ->[ok] "Create Ticket"
else
  -->[full] if "Standby?" then
    ->[ok] "Standby Ticket"
  else
    -->[no] "Cancel Ticket"
    "Cancel Ticket" --> (*)
  endif
endif

"Create Ticket" --> ==show==
"Standby Ticket" --> ==show==
==show== --> "Show Ticket"
"Show Ticket" --> (*)
```
````

``` plantuml!
(*) --> "Find Event"
"Find Event" -> "Attend Event"

if "Capacity?" then
  ->[ok] "Create Ticket"
else
  -->[full] if "Standby?" then
    ->[ok] "Standby Ticket"
  else
    -->[no] "Cancel Ticket"
    "Cancel Ticket" --> (*)
  endif
endif

"Create Ticket" --> ==show==
"Standby Ticket" --> ==show==
==show== --> "Show Ticket"
"Show Ticket" --> (*)
```

### 状态图

````
``` plantuml!
[*] --> active

active -right-> inactive : disable
inactive -left-> active  : enable

inactive --> closed  : close
active --> closed  : close

closed --> [*]
```
````

``` plantuml!
[*] --> active

active -right-> inactive : disable
inactive -left-> active  : enable

inactive --> closed  : close
active --> closed  : close

closed --> [*]
```

### 序列图

````
``` plantuml!
actor Entrant

Entrant -> Ticket : Attend Event Request

activate Ticket
Ticket -> Member : Create Member Request

activate Member
Member -> Member : Create Member
Ticket <-- Member : Create Member Response
deactivate Member

Ticket -> Ticket : Create Ticket
Entrant <-- Ticket : Attend Event Response
deactivate Ticket
```
````

``` plantuml!
actor Entrant

Entrant -> Ticket : Attend Event Request

activate Ticket
Ticket -> Member : Create Member Request

activate Member
Member -> Member : Create Member
Ticket <-- Member : Create Member Response
deactivate Member

Ticket -> Ticket : Create Ticket
Entrant <-- Ticket : Attend Event Response
deactivate Ticket
```

````
```plantuml!

@startuml
skinparam backgroundColor #EEEBDC
skinparam handwritten true

skinparam sequence {
	ArrowColor DeepSkyBlue
	ActorBorderColor DeepSkyBlue
	LifeLineBorderColor blue
	LifeLineBackgroundColor #A9DCDF
	
	ParticipantBorderColor DeepSkyBlue
	ParticipantBackgroundColor DodgerBlue
	ParticipantFontName Impact
	ParticipantFontSize 17
	ParticipantFontColor #A9DCDF
	
	ActorBackgroundColor aqua
	ActorFontColor DeepSkyBlue
	ActorFontSize 17
	ActorFontName Aapex
}

actor User
participant "First Class" as A
participant "Second Class" as B
participant "Last Class" as C

User -> A: DoWork
activate A

A -> B: Create Request
activate B

B -> C: DoWork
activate C
C --> B: WorkDone
destroy C

B --> A: Request Created
deactivate B

A --> User: Done
deactivate A

@enduml
```
````


```plantuml!

@startuml
skinparam backgroundColor #EEEBDC
skinparam handwritten true

skinparam sequence {
	ArrowColor DeepSkyBlue
	ActorBorderColor DeepSkyBlue
	LifeLineBorderColor blue
	LifeLineBackgroundColor #A9DCDF
	
	ParticipantBorderColor DeepSkyBlue
	ParticipantBackgroundColor DodgerBlue
	ParticipantFontName Impact
	ParticipantFontSize 17
	ParticipantFontColor #A9DCDF
	
	ActorBackgroundColor aqua
	ActorFontColor DeepSkyBlue
	ActorFontSize 17
	ActorFontName Aapex
}

actor User
participant "First Class" as A
participant "Second Class" as B
participant "Last Class" as C

User -> A: DoWork
activate A

A -> B: Create Request
activate B

B -> C: DoWork
activate C
C --> B: WorkDone
destroy C

B --> A: Request Created
deactivate B

A --> User: Done
deactivate A

@enduml
```

### 对像图

````
``` plantuml!
object User
object Group
object Member

object Event
object Ticket

User . Group
User o.. Member
Group o.. Member

Group o. Event
Event o.. Ticket
Member . Ticket
```
````

``` plantuml!
object User
object Group
object Member

object Event
object Ticket

User . Group
User o.. Member
Group o.. Member

Group o. Event
Event o.. Ticket
Member . Ticket
```

### 类图

````
``` plantuml!
class User {
  username
  password
  +sign_in()
}

class Group {
  name
}

class Member {
  roles
}

User .. Member
Group .. Member
```
````

``` plantuml!
class User {
  username
  password
  +sign_in()
}

class Group {
  name
}

class Member {
  roles
}

User .. Member
Group .. Member
```

### 组件图

````
``` plantuml!
cloud "Cloud" {
  package "Package" {
    [register]
    frame "frame" {
      [backup]
    }
  }
}

node "Node" {
  database "Database" {
    [store]
  }
  folder "Folder" {
    [File]
  }
}

[register] .. [store] : HTTP
[backup] .. [File] : FTP
```
````

``` plantuml!
cloud "Cloud" {
  package "Package" {
    [register]
    frame "frame" {
      [backup]
    }
  }
}

node "Node" {
  database "Database" {
    [store]
  }
  folder "Folder" {
    [File]
  }
}

[register] .. [store] : HTTP
[backup] .. [File] : FTP
```

### 时间图

````
``` plantuml!
@startuml
robust "DNS Resolver" as DNS
robust "Web Browser" as WB
concise "Web User" as WU

@0
WU is Idle
WB is Idle
DNS is Idle

@+100
WU -> WB : URL
WU is Waiting
WB is Processing

@+200
WB is Waiting
WB -> DNS@+50 : Resolve URL

@+100
DNS is Processing

@+300
DNS is Idle
@enduml
```
````

``` plantuml!
@startuml
robust "DNS Resolver" as DNS
robust "Web Browser" as WB
concise "Web User" as WU

@0
WU is Idle
WB is Idle
DNS is Idle

@+100
WU -> WB : URL
WU is Waiting
WB is Processing

@+200
WB is Waiting
WB -> DNS@+50 : Resolve URL

@+100
DNS is Processing

@+300
DNS is Idle
@enduml
```

# 疑问

# 相关

1. [plantuml 官网](http://plantuml.com//)
2. [plantuml使用参考文档(PDF格式)](http://plantuml.com/PlantUML_Language_Reference_Guide.pdf)
3. [plantuml 官方语法](http://plantuml.com/sitemap-language-specification)