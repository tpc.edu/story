---
title: 小书匠语法说明之音频
tags: markdown, 语法,音频,小书匠
slug: /grammar/feature/audio/
grammar_audio: true
---

[toc]


# 概述

音频语法用于在文章里嵌入一段音频内容。音频内容支持网络地址的音频，也支持本地上传到小书匠内置数据库里。

该语法并非 commonmark 标准语法，目前只有小书匠提供了支持。

# 使用

元数据标识: **grammar_audio**

想要使用该语法，需要在**设置>扩展语法** 里把**音频**选项打开。或者在每篇文章的元数据里通过 **grammar_audio** 进行控制。系统默认打开**音频**语法功能

音频语法书写格式主要借鉴了图片语法的书写格式，不同的是音频语法使用的是 `～` 符号代替图片语法的 `!` 符号。

## 内联式音频

### 示例

```
~[音乐](http://markdown.xiaoshujiang.com/media/horse.ogg)
```

### 效果

~[音乐](http://markdown.xiaoshujiang.com/media/horse.ogg)

## 引用式音频

### 示例

```
~[音乐][a]

[a]: http://markdown.xiaoshujiang.com/media/horse.ogg
```

### 效果

~[音乐][a]

[a]: http://markdown.xiaoshujiang.com/media/horse.ogg


# 注

小书匠编辑器是一个纯文本编辑平台，不建议将太大的音频文件上传到小书匠内置数据内。

# 相关