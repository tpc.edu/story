---
title: 小书匠语法说明之高亮标记
tags: markdown, 语法,高亮,小书匠
slug: /grammar/feature/mark/
grammar_mark: true
---

[toc]


# 概述

高亮标记语法用于对文章内文字进行着色处理，修改文字颜色及背景色。该语法不是标准的 commonmark， 大部份的编辑器及主流 markdown 文档平台也不提供支持。小书匠使用 [markdown-it][1] 的扩展 [markdown-it-mark][2] 实现对高亮标识语法的支持。同时小书匠扩展了该功能，使得用户可以自定义每个高亮区域的文字颜色。


# 使用

元数据标识: **grammar_mark**

想要使用该语法，需要在**设置>扩展语法** 里把**高亮标识**选项打开。或者在每篇文章的元数据里通过 **grammar_mark** 进行控制。系统默认打开**高亮标识**语法功能。



## 默认的高亮标识

默认高亮标识可以直接将文字包裹在 `==` 符号内就可以。系统默认以预定的 css 样式颜色显示。用户自己也可以通过自定义 css 样式，调整高亮标识显示的文字颜色和背景色。

### 示例

```
==被记号的文字==
```

### 效果

==被记号的文字==

### HTML 片段

``` html
<span class="mark">被记号的文字</span>
```

### 自定义 css 高亮颜色

```
.mark {
    background-color: rgba(221,243,231,.4);
    color: #29754d;
}
```

## 预定颜色高亮标识

小书匠提供了一种显性的设定高亮标识颜色。只要在被包裹的高亮标识文字后方加上空格和颜色值，系统就会将当前文字显示成指定的颜色值，并对背景色进行一定比例透明背景色处理。显示原理就是直接在生成的 HTML 片段里添加上颜色值样式，这样预览定义的 css 样式就会失效，取代的是内联式的颜色样式。

### 示例

```
==被预定颜色记号的文字 #f44336==
```

### 效果

==被预定颜色记号的文字 #f44336==

## HTML 片段

生成的 HTML 片段，会添加多种 class, 除了 mark, 还添加了 mark_color, mark_color_f44336

``` html
<span class="mark mark_color mark_color_f44336" style="color:#f44336;background-color:rgba(244, 67, 54, 0.1)">被预定颜色记号的文字</span>
```


### 自定义 css 样式

在该 HTML 片段里添加这么多 class 属性，主要是方便用户扩展自己需要的显示样式。比如有些用户只想高亮指定颜色的文字样式，但不要添加什么背景，利用这些 class 属性，再加上自定义 css 样式，就可以很轻松的实现。

#### 只高亮文字处理背景色

``` css
.mark.mark_color{
    background-color: init!important;
}
```

#### 只对特定颜色禁用高亮背景色

``` css
.mark.mark_color.mark_color_f44336{
    background-color: init!important;
}
```


# 疑问



# 相关

小书匠内置的 codemirror 编辑器提供了对**预定颜色高亮标识**颜色选择器，默认是打开状态，如果您发现不能选择颜色，请确认下

1. 是否正确的高亮语法
2. 是否使用 codemirror 内置编辑器
3. 是否在`设置>编辑器>codemirror` 里打开了颜色选择器功能。


1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)


[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-mark