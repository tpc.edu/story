---
title: 小书匠语法说明之远程图片
slug: /grammar/feature/xsjimg/
tags: markdown, 语法,xsjimg,小书匠
grammar_xsjimg: true
---

[toc]


# 概述

将代码段内的文字转换成 img 标签

远程图片语法用于显示那些支持动态生成图片的服务在小书匠编辑器上显示这些动态图片，比如 [Gravizo][1]。该语法不是标准的 commonmark， 目前只有小书匠提供了对该语法的支持。



# 使用

元数据标识: **grammar_xsjimg**

想要使用该语法，需要在**设置>扩展语法** 里把**xsjimg**选项打开。或者在每篇文章的元数据里通过 **grammar_xsjimg** 进行控制。系统默认关闭**xsjimg**语法功能。



## 参数设置

### 服务器地址 (url)

支持的变量为 `xsjimg`,

示例

服务器地址为: https://g.gravizo.com/svg?{{xsjimg}}

### 参数编码方式 (encode)

支持的类型有: default, base64, zip

default
: 使用 encodeURIComponent(正文内容) 方法对正文进行编码，并将编码后的内容替换到服务器地址里的 `xsjimg` 变量里

base64
: 使用 btoa(encodeURIComponent(正文内容)) 方法对正文进行编码，并将编码后的内容替换到服务器地址里的 `xsjimg` 变量里

zip
: 使用 压缩算法对正文进行压缩，并将编码后的内容替换到服务器地址里的 `xsjimg` 变量里

## 代码块参数

xsjimg 支持修改参数，比如修改 url 参数或者 encode 参数， 需要注意的是参数值必须进行 encodeURIComponent 编码，例如

````
```xsjimg!url=https%3A%2F%2Fg.gravizo.com%2Fsvg%3F%7B%7Bxsjimg%7D%7D
````

这样的话，就会替换掉全局的url,而仅对该代码块使用指定的 url 参数



## 支持 xsjimg 语法的相关网站服务

### Gravizo

http://gravizo.com/

url 变量 `https://g.gravizo.com/svg?{{xsjimg}}` , 
编码后的变量 `https%3A%2F%2Fg.gravizo.com%2Fsvg%3F%7B%7Bxsjimg%7D%7D`


### texs2cms

https://tex.s2cms.com/

url 变量 `https://tex.s2cms.ru/svg/{{xsjimg}}`, 
编码后的变量 `https%3A%2F%2Ftex.s2cms.ru%2Fsvg%2F%7B%7Bxsjimg%7D%7D`




# gravizo

## 示例



````
``` xsjimg!?url=https%3A%2F%2Fg.gravizo.com%2Fsvg%3F%7B%7Bxsjimg%7D%7D
/**
*Structural Things
*@opt commentname
*@note Notes can
*be extended to
*span multiple lines
*/
class Structural{}

/**
*@opt all
*@note Class
*/
class Counters extends Structural {
        static public int counter;
        public int getCounter();
}

/**
*@opt shape activeclass
*@opt all
*@note Active Class
*/
class RunningCounter extends Counter{}
```
````

## 预览效果

``` xsjimg!?url=https%3A%2F%2Fg.gravizo.com%2Fsvg%3F%7B%7Bxsjimg%7D%7D
/**
*Structural Things
*@opt commentname
*@note Notes can
*be extended to
*span multiple lines
*/
class Structural{}

/**
*@opt all
*@note Class
*/
class Counters extends Structural {
        static public int counter;
        public int getCounter();
}

/**
*@opt shape activeclass
*@opt all
*@note Active Class
*/
class RunningCounter extends Counter{}
```

# texs2cms

## 示例

````
``` xsjimg!?url=https%3A%2F%2Ftex.s2cms.ru%2Fsvg%2F%7B%7Bxsjimg%7D%7D
\begin{tikzpicture}[scale=1.0544]\small
\begin{axis}[axis line style=gray,
	samples=120,
	width=9.0cm,height=16.4cm,
	xmin=-1.5, xmax=1.5,
	ymin=0, ymax=1.8,
	restrict y to domain=-0.2:2,
	ytick={1},
	xtick={-1,1},
	axis equal,
	axis x line=center,
	axis y line=center,
	xlabel=$x$,ylabel=$y$]
\addplot[red,domain=-2:1,semithick]{exp(x)};
\addplot[black]{x+3};
\addplot[] coordinates {(1,1.5)} node{$y=x+1$};
\addplot[red] coordinates {(-1,0.6)} node{$y=e^x$};
\path (axis cs:0,0) node [anchor=north west,yshift=-0.07cm] {0};
\end{axis}
\end{tikzpicture}
```
````

## 预览效果图

``` xsjimg!?url=https%3A%2F%2Ftex.s2cms.ru%2Fsvg%2F%7B%7Bxsjimg%7D%7D
\begin{tikzpicture}[scale=1.0544]\small
\begin{axis}[axis line style=gray,
	samples=120,
	width=9.0cm,height=16.4cm,
	xmin=-1.5, xmax=1.5,
	ymin=0, ymax=1.8,
	restrict y to domain=-0.2:2,
	ytick={1},
	xtick={-1,1},
	axis equal,
	axis x line=center,
	axis y line=center,
	xlabel=$x$,ylabel=$y$]
\addplot[red,domain=-2:1,semithick]{exp(x)};
\addplot[black]{x+3};
\addplot[] coordinates {(1,1.5)} node{$y=x+1$};
\addplot[red] coordinates {(-1,0.6)} node{$y=e^x$};
\path (axis cs:0,0) node [anchor=north west,yshift=-0.07cm] {0};
\end{axis}
\end{tikzpicture}
```

# 注

目前小书匠不提供相关的解析服务器，使用前需要自己指定一个服务器


# 相关


1. [gravizo 官网][1]
2. [texs2cms 官网][2]

[1]: http://g.gravizo.com/
[2]: https://tex.s2cms.com/