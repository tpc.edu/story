---
title: 小书匠语法说明之emoji表情
tags: markdown, 语法,emoji,小书匠
slug: /grammar/advance/emoji/
grammar_emoji: true
---

[toc]


# 概述

emoji表情语法用于显示 emoji表情符号。该语法并非 commonmark 标准语法，但大部份编辑器及markdown 博客平台都会 emoji 表情提供了支持，小书匠也提供了对 emoji 表情的支持。

小书匠使用 [markdown-it][1] 的扩展 [markdown-it-emoji][2] 实现对emoji表情的支持。

# 使用

元数据标识: **grammar_emoji**

提供emoji表情语法扩展功能，支持的 emoji 符号可以参考这里


想要使用该语法，需要在**设置>扩展语法** 里把**emoji**选项打开。或者在每篇文章的元数据里通过 **grammar_emoji** 进行控制。系统默认关闭了emoji表情语法功能

书写格式

```
:表情符号对应的单词:
```

同时支持缩写格式的表情

```
:-) :-( 8-) ;)
```


## 示例

```
  :wink:   :cry: :laughing: :yum:
```

## 效果

  :wink:  :cry: :laughing: :yum:
  
>  普通表情: :wink: :crush: :cry: :tear: :laughing: :yum:  
>  
>  
> 缩写的表情 (emoticons): :-) :-( 8-) ;)

# 缩写表情

支持的缩写表情有

```
module.exports = {
  angry:            [ '>:(', '>:-(' ],
  blush:            [ ':")', ':-")' ],
  broken_heart:     [ '</3', '<\\3' ],
  // :\ and :-\ not used because of conflict with markdown escaping
  confused:         [ ':/', ':-/' ], // twemoji shows question
  cry:              [ ":'(", ":'-(", ':,(', ':,-(' ],
  frowning:         [ ':(', ':-(' ],
  heart:            [ '<3' ],
  imp:              [ ']:(', ']:-(' ],
  innocent:         [ 'o:)', 'O:)', 'o:-)', 'O:-)', '0:)', '0:-)' ],
  joy:              [ ":')", ":'-)", ':,)', ':,-)', ":'D", ":'-D", ':,D', ':,-D' ],
  kissing:          [ ':*', ':-*' ],
  laughing:         [ 'x-)', 'X-)' ],
  neutral_face:     [ ':|', ':-|' ],
  open_mouth:       [ ':o', ':-o', ':O', ':-O' ],
  rage:             [ ':@', ':-@' ],
  smile:            [ ':D', ':-D' ],
  smiley:           [ ':)', ':-)' ],
  smiling_imp:      [ ']:)', ']:-)' ],
  sob:              [ ":,'(", ":,'-(", ';(', ';-(' ],
  stuck_out_tongue: [ ':P', ':-P' ],
  sunglasses:       [ '8-)', 'B-)' ],
  sweat:            [ ',:(', ',:-(' ],
  sweat_smile:      [ ',:)', ',:-)' ],
  unamused:         [ ':s', ':-S', ':z', ':-Z', ':$', ':-$' ],
  wink:             [ ';)', ';-)' ]
};
```

# 疑问

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)
3. [markdown-it-emoji 扩展插件][2]
4. [twemoji 表情][3]
5. [twemoji github 库][4]

[1]: https://github.com/markdown-it/markdown-it
[2]: https://github.com/markdown-it/markdown-it-emoji
[3]: https://twemoji.twitter.com/
[4]: https://github.com/twitter/twemoji