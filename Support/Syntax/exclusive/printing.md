---
title: 小书匠语法说明之印刷字
tags: markdown, 语法,印刷字,小书匠
grammar_typographer: true
slug: /grammar/feature/typographer/
---

[toc]


# 概述

**印刷字**语法用于在文章内替换掉一些不太容易输入的字符。该语法不是标准的 commonmark， 大部份的编辑器及主流 markdown 文档平台也不被支持，但像 pandoc 等解析工具提供了对其支持。小书匠使用 [markdown-it][1] 实现对该功能的支持。


# 使用

元数据标识: **grammar_typographer**

想要使用该语法，需要在**设置>扩展语法** 里把**印刷字**选项打开。或者在每篇文章的元数据里通过 **grammar_typographer** 进行控制。系统默认关闭**印刷字**语法功能

系统默认只支持下面这几种字的替换

```
(c) (C) (r) (R) (tm) (TM) (p) (P) +-
```

## 印刷字替换

系统将自动替换下列文字，转换成排版系统使用的符号

## 示例

```
(c) (C) (r) (R) (tm) (TM) (p) (P) +-
```

## 效果

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

# 疑问

# 相关

1. [markdown-it github官网][1]
2. [markdown-it 语法测试](https://markdown-it.github.io/)


[1]: https://github.com/markdown-it/markdown-it