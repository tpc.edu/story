---
title: 小书匠语法说明之统计图
tags: markdown, 语法,统计图,小书匠
slug: /grammar/feature/plot/
grammar_plot: true
---

[toc]


# 概述

统计图语法使用 [flot][1] 插件在文章内显示统计图。该语法不是标准的 commonmark 语法， 目前只有小书匠提供了对该语法的支持

# 使用

元数据标识: **grammar_plot**

想要使用该语法，需要在**设置>扩展语法** 里把**统计图**选项打开。或者在每篇文章的元数据里通过 **grammar_plot** 进行控制。系统默认关闭**统计图**语法功能

详细语法，可以参考这里.  https://github.com/flot/flot/blob/master/API.md

数据格式为：`{"data": [], "options":{}}`
系统使用[jquery.parseJSON()](http://api.jquery.com/jquery.parsejson/)函数进行解析，因此代码必须符合该函数的要求才能正常解析。


## 示例

````
```plot!
{
"data": [ [[0, 0], [1, 1]] ],
"options": { "yaxis": { "max": 1 } }
}
```
````

## 效果

```plot!
{
"data": [ [[0, 0], [1, 1]] ],
"options": { "yaxis": { "max": 1 } }
}
```

# 疑问

# 相关

1. [flot 官网][1]

[1]: https://github.com/flot/flot/