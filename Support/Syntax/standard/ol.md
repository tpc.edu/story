---
title: 小书匠语法说明之有序列表
tags: markdown, 语法,基础语法,有序列表,小书匠
grammar_code: true
slug: /grammar/common/ol/
---

[toc]


# 概述

有序列表用来对一系列条目进行编号显示。除了常规的编号功能外，小书匠还提供了起始编号值设定功能。

# 使用

## 默认有序列表

直接在每个条目上进行数值编号

### 示例

```
>>==1. ==<<条目一
>>==2. ==<<条目二
>>==3. ==<<条目三
```

### 效果

1. 条目一
2. 条目二
3. 条目三

## 自动有序列表编号

如果在同一个段落的多个条目里，编号值不是顺序的，系统也会自动纠正成顺序的数字编号，并以第一个条目的编号值做为起始值

### 示例

```?fancy=2
1. 条目一
>>==1. ==<<条目二
3. 条目三
```

### 效果

1. 条目一
1. 条目二
3. 条目三

## 指定有序列表起始编号值

在同一个段落的多个条目里，可以对第一个编号值指定数值大小，生成的文档将按照该值进行编号

### 示例

```?fancy=1
>>==5. ==<<条目一
1. 条目二
3. 条目三
```

### 效果

5. 条目一
1. 条目二
3. 条目三

## 嵌套有序列表

通过间隔二个以上的空隔，可以实现多级嵌套有序列表功能。

### 示例

```?fancy=2,3
1. 条目一
>>==	==<<1. 子条目一
>>==	==<<2. 子条目二
2. 条目二
```

## 单条目多行

列表项目标记通常是放在最左边，但是其实也可以缩进，最多 3 个空格，项目标记后面则一定要接着至少一个空格或制表符。

要让列表看起来更漂亮，你可以把内容用固定的缩进整理好：

```
1. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
   Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
   viverra nec, fringilla in, laoreet vitae, risus.
2. Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
  Suspendisse id sem consectetuer libero luctus adipiscing.
```

或者也可以写成这样

```
1. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
viverra nec, fringilla in, laoreet vitae, risus.
2. Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
Suspendisse id sem consectetuer libero luctus adipiscing.
```

### 显示效果

1. 条目一
    1. 子条目一
	2. 子条目二
2. 条目二

## HTML

像下面的有序列表

```
1.  Bird
2.  McHale
3.  Parish
```
生成对应的 HTML 结构为

```html
<ol>
<li>Bird</li>
<li>McHale</li>
<li>Parish</li>
</ol>
```

如果指定了起始编辑

```
4.  Bird
2.  McHale
3.  Parish
```

生成对应的 HTML 结构为


``` html
<ol start="4">
<li>
Bird</li>
<li>
McHale</li>
<li>
Parish</li>
</ol>
```

如果列表项目间用空行分开，在输出 HTML 时 Markdown 就会将项目内容用 `<p>` 标签包起来， 比如：

```
1.  Bird

2.  McHale
```

生成对应的 HTM 为：

```` html
<ol>
<li>
<p>Bird</p>
</li>
<li>
<p>McHale</p>
</li>
</ol>
````

## 有序列表与其他语法的使用

### 列表项目多段落

列表项目可以包含多个段落，每个项目下的段落都必须缩进 4 个空格或是 1 个制表符

```
1. This is a list item with two paragraphs. Lorem ipsum dolor
	sit amet, consectetuer adipiscing elit. Aliquam hendrerit
	mi posuere lectus.

	Vestibulum enim wisi, viverra nec, fringilla in, laoreet
	vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
	sit amet velit.

2. Suspendisse id sem consectetuer libero luctus adipiscing.

```

如果你每行都有缩进，看起来会看好很多，当然，再次地，如果你很懒惰，Markdown 也允许：

```
1. This is a list item with two paragraphs.

   This is the second paragraph in the list item. You're
only required to indent the first line. Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.

2. Another item in the same list.
```

### 列表和引用 

如果要在列表项目内放进引用，那 `>` 就需要缩进

```
1. A list item with a blockquote:

	> This is a blockquote
	> inside a list item.
```


### 列表和代码块

如果要放代码区块的话，该区块就需要缩进两次，也就是 8 个空格或是 2 个制表符：

```
1. 一列表项包含一个列表区块：

	<代码写在这>
```

或者

````
1. 一列表项包含一个列表区块：
  ``` javascript
   var x = 1;
  ```
````

显示效果

*   一列表项包含一个列表区块：
     ``` javascript
      var x = 1;
	```

# 疑问

当然，项目列表很可能会不小心产生，像是下面这样的写法

```
1986. What a great season.
```

换句话说，也就是在行首出现数字-句点-空白，要避免这样的状况，你可以在句点前面加上反斜杠。

```
1986\. What a great season.
```

# 相关

1. [无序列表语法](/docs/grammar/common/ul/)
2. [待办列表语法](/docs/grammar/advance/todo/)
2. 参考了 [markdown 语法说明](http://www.markdown.cn/#lists)

