---
title: 小书匠语法说明之引用
tags: markdown, 语法,基础语法,引用,小书匠
slug: /grammar/common/quote/
---

[toc]


# 概述

Markdown 标记引用是使用类似 email 中用 > 的引用方式。如果你还熟悉在 email 信件中的引言部分，你就知道怎么在 Markdown 文件中建立一个区块引用。

系统支持单行引用，多行引用，多级引用等标准语法。

# 使用

## 单行引用

在引用的文字前面添加符号 `>`。

### 示例

```
> 这是一个单行引用
```

### 效果

> 这是一个单行引用

## 多行引用

多行引用可以只在第一行里使用 `>` 符号，只要是在同一个段落内，系统都会把该段落处理成引用。同时也可以在段落的每一个起始行里添加 `>` 符号。

### 示例

```
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.
```

或者比较简单的写法

```
> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.

> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
id sem consectetuer libero luctus adipiscing.
```



### 效果

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
> 
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.


> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.

> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
id sem consectetuer libero luctus adipiscing.

## 多级引用

连继多个 `>` 符号，就可以实现多级引用。

### 示例

```
> 这是一个多级引用
> 一级引用
>> 二级引用
>>> 三级引用
```

### 效果

> 这是一个多级引用
> 一级引用
>> 二级引用
>>> 三级引用

## 引用与其他语法同时使用

在引用里可以使用其他 markdown 语法

### 示例

```
> ## 这是一个标题。
> 
> 1.   这是第一行列表项。
> 2.   这是第二行列表项。
> 
> 给出一些例子代码：
> 
>     return shell_exec("echo $input | $markdown_script");
```

### 效果

> ## 这是一个标题。
> 
> 1.   这是第一行列表项。
> 2.   这是第二行列表项。
> 
> 给出一些例子代码：
> 
>     return shell_exec("echo $input | $markdown_script");

# 疑问

# 相关