```
title: SHORTCUT KEYS
```
# 小书匠语法说明之HTML
[TOC]
## 概述
Markdown 语法的目标是：成为一种适用于网络的书写语言。

Markdown 标记语言的目的不是替代 HTML，也不是发明一种更便捷的插入 HTML 标签的方式。它对应的只是 HTML 标签的一个很小的子集。

当 markdown 提供的格式不能满足用户排版的需求时，允许在 markdown 正文里使用 HTML 标签进行更复杂的排版需求，实现更个性化的文档效果。

## 使用
元数据标识: **grammar_html**

想要使用该语法，需要在**设置**>**扩展语法** 里把**HTML**选项打开。或者在每篇文章的元数据里通过 **grammar_html** 进行控制。系统默认打开**HTML**语法功能

### HTML
```
<div class="hey">Hello world</div> 
```
### 示例
```
<div class="hey">Hello world</div> 
```
### 效果
<div class="hey">Hello world</div> 

## 疑问

## 相关