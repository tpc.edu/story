---
title: 小书匠语法说明之段落
tags: markdown, 语法,基础语法, 段落,小书匠
slug: /grammar/common/paragraph/
---

[toc]


# 概述

# 使用

一个 Markdown 段落是由一个或多个连续的文本行组成，它的前后要有一个以上的空行（空行的定义是显示上看起来像是空的，便会被视为空行。比方说，若某一行只包含空格和制表符，则该行也会被视为空行）。普通段落不该用空格或制表符来缩进。

「由一个或多个连续的文本行组成」这句话其实暗示了 Markdown 允许段落内的强迫换行（插入换行符），这个特性和其他大部分的 text-to-HTML 格式不一样（包括 Movable Type 的「Convert Line Breaks」选项），其它的格式会把每个换行符都转成 <br /> 标签。

如果你确实想要依赖 Markdown 来插入 <br /> 标签的话，在插入处先按入两个以上的空格然后回车。

的确，需要多费点事（多加空格）来产生 <br /> ，但是简单地「每个换行都转换为 <br />」的方法在 Markdown 中并不适合， Markdown 中 email 式的 区块引用 和多段落的 列表 在使用换行来排版的时候，不但更好用，还更方便阅读。



## 示例

```?linenums&fancy=2&title=单独一个空行
段落一

段落二
```

```?linenums&fancy=2,3,4&title=多个空行
段落一



段落二
```



## 效果

展示一张图片

## html

为了实现更个性化的显示效果，小书匠对每个段落添加了 class。

```html
<p class="xsj_paragraph xsj_paragraph_level_0"></p>
```

如果段落处在列表或者引用内，段落的 class 内容会进行相应的变化，根据自己所处在的嵌套层级，显示不同的 class 名称。

比如 

```
hello

> hello
```

生成的 html

```
<p class="xsj_paragraph xsj_paragraph_level_0">hello</p>
<blockquote>
<p class="xsj_paragraph xsj_paragraph_level_1">hello</p>
</blockquote>
```

# 疑问

# 相关

## 如何控制段落首先缩进

可以通过自定义 css 样式

``` css
.xsj_paragraph_level_0{
    text-indent: 2em;
}
```