---
title: 小书匠语法说明之无序列表
tags: markdown, 语法,基础语法,无序列表,小书匠
slug: /grammar/common/ul/
---

[toc]


# 概述

无序列表用来对不需要进行编号的条目进行列表显示。

# 使用

## 单级无序列表

在每一个条目前面添加`*`, `-` 或者 `+` 符号，就可以实现无序列表

### 示例

使用 `*` 创建的无序列表

```
* 无序条目
* 无序条目
* 无序条目
```

使用 `-` 创建的无序列表

```
- 无序条目
- 无序条目
- 无序条目
```

使用 `+` 创建的无序列表

```
+ 无序条目
+ 无序条目
+ 无序条目
```

### 效果


使用 `*` 创建的无序列表

* 无序条目
* 无序条目
* 无序条目

使用 `-` 创建的无序列表

- 无序条目
- 无序条目
- 无序条目

使用 `+` 创建的无序列表

+ 无序条目
+ 无序条目
+ 无序条目


## 多级嵌套无序列表

通过间隔二个以上的空隔，可以实现多级嵌套无序列表功能。

### 示例

```
+ 无序条目
	+ 子无序条目
	+ 子无序条目
		- 孙无序条目
		- 孙无序条目
+ 无序条目
	* 子无序条目
	* 子无序条目
+ 无序条目
```

### 效果

+ 无序条目
	+ 子无序条目
	+ 子无序条目
		- 孙无序条目
		- 孙无序条目
+ 无序条目
	* 子无序条目
	* 子无序条目
+ 无序条目

## 单条目多行

列表项目标记通常是放在最左边，但是其实也可以缩进，最多 3 个空格，项目标记后面则一定要接着至少一个空格或制表符。

要让列表看起来更漂亮，你可以把内容用固定的缩进整理好：

```
* Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
   Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
   viverra nec, fringilla in, laoreet vitae, risus.
* Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
  Suspendisse id sem consectetuer libero luctus adipiscing.
```

或者也可以写成这样

```
* Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
viverra nec, fringilla in, laoreet vitae, risus.
* Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
Suspendisse id sem consectetuer libero luctus adipiscing.
```

## HTML

像下面的无序列表

```
* Bird
* McHale
* Parish
```
生成对应的 HTML 结构为

```html
<ul>
<li>Bird</li>
<li>McHale</li>
<li>Parish</li>
</ul>
```

如果列表项目间用空行分开，在输出 HTML 时 Markdown 就会将项目内容用 `<p>` 标签包起来， 比如：

```
* Bird

* McHale
```

生成对应的 HTM 为：

```` html
<ul>
<li>
<p>Bird</p>
</li>
<li>
<p>McHale</p>
</li>
</ul>
````

## 无序列表与其他语法的使用

### 列表项目多段落

列表项目可以包含多个段落，每个项目下的段落都必须缩进 4 个空格或是 1 个制表符

```
* This is a list item with two paragraphs. Lorem ipsum dolor
  sit amet, consectetuer adipiscing elit. Aliquam hendrerit
  mi posuere lectus.

  Vestibulum enim wisi, viverra nec, fringilla in, laoreet
  vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
  sit amet velit.

* Suspendisse id sem consectetuer libero luctus adipiscing.

```

如果你每行都有缩进，看起来会看好很多，当然，再次地，如果你很懒惰，Markdown 也允许：

```
* This is a list item with two paragraphs.

  This is the second paragraph in the list item. You're
only required to indent the first line. Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.

* Another item in the same list.
```

### 列表和引用 

如果要在列表项目内放进引用，那 `>` 就需要缩进

```
* A list item with a blockquote:
  > This is a blockquote
  > inside a list item.
```


### 列表和代码块

如果要放代码区块的话，该区块就需要缩进两次，也就是 8 个空格或是 2 个制表符：

```
* 一列表项包含一个列表区块：
    <代码写在这>
```

或者

````
* 一列表项包含一个列表区块：
  ``` javascript
  var x = 1;
  ```
````

显示效果

*   一列表项包含一个列表区块：
     ``` javascript
      var x = 1;
	```


# 疑问

# 相关

1. [有序列表](/docs/grammar/common/ol/)
2. [待办列表](/docs/grammar/advance/todo/)
2.  参考了 [markdown 语法说明](http://www.markdown.cn/#lists)