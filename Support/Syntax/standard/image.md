```
title: SHORTCUT KEYS
```
# 小书匠语法说明之图片
[TOC]
## 概述
很明显地，要在纯文字应用中设计一个「自然」的语法来插入图片是有一定难度的。

Markdown 使用一种和链接很相似的语法来标记图片，同样也允许两种样式： 内联式和引用式

## 使用
在超链接的写法前加一个 `!`，就是引用图片的方法

### 内联式图片
#### 示例
```
![小书匠图片](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png) 
```
#### 效果
![小书匠图片](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png) 
### 引用式图片
#### 示例
```
![小书匠图片][a] 
 
[a]: http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png 
```
#### 效果
![小书匠图片][a] 
 
[a]: http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png 
## 高级功能
### 块级图片
虽然图片语法生成的 html 片段为行内行为，但小书匠系统对该语法进行了加强，如果图片语法的上下各空一行，小书匠就会将该图片做为特殊的块级语法显示，并将图片里的描述文字在图片下方进行显示。

#### 如何打开块级图片
系统默认是打开块级图片功能的，当然用户也可以自定义是否使用块级图片功能。当打开了块级图片功能后，用户只要在每张图片的上下各空一行，并且上下行除了图片没有其他什么文字内容。

下面列出了二种方法来进行块级图片的控制

* 通过文章的元数据 renderImgBlock 进行控制
* 通过 小书匠主按钮>设置>预览>是否自动块级图片 设置
#### 示例
```
段落一 
 
![小书匠图片](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png) 
 
段落二
```
### 块级图片描述
小书匠除了提供块级图片功能外，用户还可以在图片下方显示一个描述性文字。

#### 如何打开块级图片标题描述
系统默认是打开自动填充块级图片标题的，用户也可以自定义控制是否打开图片标题描述。

通过 小书匠主按钮>设置>预览>是否自动填充块级图片标题 设置
通过对每篇文章元数据 renderWrapImgTitle 进行控制
当用户打开了块级图片描述功能后，系统就会把用户在图片内的 alt 文字 或者 title 文字显示到图片的下方，如果用户同时设置了 alt 和 title 文字，则系统自动取 title 文字做为描述性文字。

#### 什么是 alt 和 title
alt
```
![alt 文字](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png) 
```
title
```
![](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png "title 文字") 
```
alt and title
```
![alt 文字](http://markdown.xiaoshujiang.com/img/avatar_d_white_bg_256x256.png "title 文字") 
```
### 图片附件
大部份的图片使用的扩展名为 jpg, jpeg, png, gif等，除此之外，可以认为是非图片文件，小书匠提供一种非图片文件显示为附件的兼容功能。

#### 如何打开图片附件功能
通过 小书匠主按钮>设置>预览>是否自动转换非图片扩展名为附件 选项控制
在每篇文章的元数据 renderAutoDetectAttachment 进行控制
当打开了图片附件功能后，系统会自动检测提供的图片链接是否满足图片的要求，如果为非图片，就显示成附件效果。

## 疑问
## 相关
1. 图片资源位置解析原理
3. 表格组件
4. 绘图组件
5. 图片涂鸦
6. 图片粘贴，剪切板粘贴
7. gif 图片