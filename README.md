```
title: SHORTCUT KEYS
```

# 参考

小书匠[文档](http://soft.xiaoshujiang.com/docs/)

## 小书匠语法

### [标准语法](http://soft.xiaoshujiang.com/docs/grammar/common/italic/)

### [高级语法](http://soft.xiaoshujiang.com/docs/grammar/advance/table/)

### [小书匠专用语法](http://soft.xiaoshujiang.com/docs/grammar/feature/mindmap/)

## [使用教程](http://soft.xiaoshujiang.com/docs/tutorial/store/)

